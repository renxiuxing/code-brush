package com.cn.codebrush;

import static com.cn.codebrush.排序.冒泡排序.bubble;
import static com.cn.codebrush.排序.快速排序.quick;
import static com.cn.codebrush.排序.计数排序.countSort;
import static com.cn.codebrush.排序.选择排序.selectSort;

/**
 * @Author Boolean
 * @Date 2022/12/8 23:39
 * @Version 1.0
 */
public class Test {
    // 对数器方法


    // for test
    public static void main(String[] args) {
        int testTime = 500009;
        int maxSize = 10;
        int maxValue = 100;
        boolean succeed = true;
        for (int i = 0; i < testTime; i++) {
            int[] arr1 = generateRandomArray(maxSize, maxValue);
            int[] arr2 = copyArray(arr1);
            bubble(arr1);  //方法一
            selectSort(arr2);  //方法二
            if (!isEqual(arr1, arr2)) {
                // 打印arr1
                // 打印arr2
                succeed = false;
                break;
            }
        }
        System.out.println(succeed ? "Nice!" : "Fucking fucked!");
        int[] arr = generateRandomArray(maxSize, maxValue);
        /*printArray(arr);
        insertionSort(arr);
        printArray(arr);*/
    }


    // for test
    public static int[]generateRandomArray(int maxSize,int maxValue){
        // Math.random() -> [0,1) 所有的小数，等概率返回一个
        // Math.random()* N -> [0,N) 所有小数，等概率返回一个
        // (int)(Math.random() * N) ->[0,N-1] 所有的整数，等概率返回一个
        int[] arr = new int[(int) ((maxSize + 1) * Math.random())]; // 长度随机
        for (int i = 0; i < arr.length; i++){
          arr[i] = (int) ((maxValue + 1) * Math.random()) - (int) (maxValue * Math.random());
        }
        return arr;
    }

    // for test
    public static int[]copyArray(int[] arr) {
        if (arr == null) {
            return null;
        }
        int[] res = new int[arr.length];
        for (int i = 0; i< arr.length; i++) {
            res[i] = arr[i];
        }
        return res;
    }

    // for test
    public static boolean isEqual(int[] arr1,int[] arr2){
        if ((arr1 == null && arr2 != null) || (arr1 != null && arr2 == null)){
            return false;
        }
        if (arr1 == null && arr2 == null){
            return true;
        }
        if (arr1.length != arr2.length) {
            return false;
        }
        for(int i=0;i< arr1.length; i++){
            if (arr1[i] != arr2[i]) {
                return false;
            }
        }
        return true;
    }


}
