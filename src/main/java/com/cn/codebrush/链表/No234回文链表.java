package com.cn.codebrush.链表;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author Boolean
 * @Date 2022/6/15 20:01
 * @Version 1.0
 */
public class No234回文链表 {
    public static void main(String[] args) {

    }

    public boolean isPalindrome(ListNode head) {
        List list = new ArrayList();
        int count = 0;
        ListNode temp = head;
        while(temp != null){
            list.add(temp.val);
            count++;
            temp = temp.next;
        }

        int left=0,right=list.size()-1;
        while(left<right){
            if(list.get(left) != list.get(right)){
                return false;
            }
            left++;
            right--;
        }

        return true;
    }
}
