package com.cn.codebrush.链表;

/**
 * @Author Boolean
 * @Date 2022/5/19 16:48
 * @Version 1.0
 */
public class No328奇偶链表 {
    public static void main(String[] args) {
        ListNode l1 = new ListNode(1);
        ListNode node1 = new ListNode(2);
        ListNode node2 = new ListNode(3);
        ListNode node3 = new ListNode(4);
        l1.next = node1;
        node1.next=node2;
        node2.next=node3;
        System.out.println(oddEvenList(l1));
    }


    /**
     * AC
     * @param head
     * @return
     */
    public static ListNode oddEvenList(ListNode head) {
        if(head == null){
            return null;
        }

        ListNode newNode = new ListNode();  //生成一个新的链表
        ListNode tempnewNode = newNode;    //生成一个临时的进行操作，操作的时候这个临时的是当前最新的一个，原来的链表一直保持
        ListNode oldNode = head;
        while (oldNode != null && oldNode.next != null){
            ListNode temp = oldNode.next;
            oldNode.next = oldNode.next.next;
            oldNode = oldNode.next;
            tempnewNode.next = new ListNode(temp.val);
            tempnewNode = tempnewNode.next;
        }

        ListNode tempNode = head;
        while(tempNode.next != null){
            tempNode = tempNode.next;
        }
        tempNode.next = newNode.next;

        return head;
    }
}
