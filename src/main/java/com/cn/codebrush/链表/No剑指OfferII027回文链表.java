package com.cn.codebrush.链表;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author Boolean
 * @Date 2022/5/18 21:39
 * @Version 1.0
 */
public class No剑指OfferII027回文链表 {
    public static void main(String[] args) {
        ListNode l1 = new ListNode(1);
        ListNode node1 = new ListNode(2);
        ListNode node2 = new ListNode(1);
        l1.next = node1;
        node1.next=node2;

        System.out.println(isPalindrome(l1));
    }

    public static boolean isPalindrome(ListNode head) {
        List<Integer> list = new ArrayList<>();
        while(head != null){
            list.add(head.val);
            head = head.next;
        }
        int i=0,j=list.size()-1;
        while(i<j){
            if(list.get(i) != list.get(j)){
                return false;
            }
            i++;
            j--;
        }


        return true;
    }
}
