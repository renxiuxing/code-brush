package com.cn.codebrush.链表;

/**
 * @Author Boolean
 * @Date 2022/6/22 15:34
 * @Version 1.0
 */
public class No61旋转链表 {
    public static void main(String[] args) {
        ListNode l1 = new ListNode(1);
        ListNode node1 = new ListNode(2);
        ListNode node2 = new ListNode(3);
        ListNode node3 = new ListNode(4);
        ListNode node4 = new ListNode(5);
        l1.next = node1;
        node1.next=node2;
        /*node2.next=node3;
        node3.next=node4;*/

        System.out.println(rotateRight(l1,2));
    }

    public static ListNode rotateRight(ListNode head, int k) {
        int len = 0;
        if(head == null || head.next == null || k == 0){
            return head;
        }
        ListNode temp = head;
        while(temp != null){
            len++;
            temp = temp.next;
        }
        int q = k%len;
        if(q == 0){
            return head;
        }
        ListNode preNode = head;
        ListNode nextNode = head;
        ListNode res = nextNode;
        for(int i=0;i<len-1;i++){
            if(i<len-q){
                preNode = nextNode;
                res = nextNode.next;
            }
            nextNode = nextNode.next;
        }

        preNode.next = null;
        nextNode.next = head;

        return res;

    }
}
