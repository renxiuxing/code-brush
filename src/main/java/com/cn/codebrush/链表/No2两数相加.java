package com.cn.codebrush.链表;

public class No2两数相加 {
    public static void main(String[] args) {
        ListNode l1 = new ListNode(2);
        ListNode node1 = new ListNode(4);
        ListNode node2 = new ListNode(3);
        l1.next = node1;
        node1.next=node2;


        ListNode l2 = new ListNode(5);
        ListNode node_1 = new ListNode(6);
        ListNode node_2 = new ListNode(4);

        l2.next = node_1;
        node_1.next=node_2;

        System.out.println(addTwoNumbers(l1,l2));

    }

    public static ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ListNode temp = new ListNode();
        ListNode res = temp;
        int y = 0;
        int k = 0;
        while(l1 != null || l2 != null){
            int sum = (l1==null?0:l1.val) + (l2==null?0:l2.val) + k;
            y = sum % 10;
            k = sum / 10;

            temp.next = new ListNode(y);
            temp = temp.next;

            if(l1 != null){
                l1 = l1.next;
            }
            if(l2 != null){
                l2 = l2.next;
            }
        }

        if(k != 0){
            temp.next = new ListNode(k);
        }

        return res.next;

    }

}
