package com.cn.codebrush.链表;

/**
 * @Author Boolean
 * @Date 2022/6/15 20:10
 * @Version 1.0
 */
public class No206反转链表 {
    public static void main(String[] args) {

    }

    public ListNode reverseList(ListNode head) {
        ListNode res = null;
        while(head != null){
            ListNode temp = head.next;
            head.next = res;
            res = head;
            head = temp;
        }
        return res;
    }
}
