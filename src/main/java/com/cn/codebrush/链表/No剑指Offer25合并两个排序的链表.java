package com.cn.codebrush.链表;

/**
 * @Author Boolean
 * @Date 2022/5/18 22:20
 * @Version 1.0
 */
public class No剑指Offer25合并两个排序的链表 {
    public static void main(String[] args) {
        ListNode l1 = new ListNode(1);
        ListNode node1 = new ListNode(2);
        ListNode node2 = new ListNode(4);
        l1.next = node1;
        node1.next=node2;


        ListNode l2 = new ListNode(1);
        ListNode node_1 = new ListNode(3);
        ListNode node_2 = new ListNode(4);

        l2.next = node_1;
        node_1.next=node_2;

        System.out.println(mergeTwoLists(l1,l2));
    }


    public static ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        ListNode res = new ListNode();
        ListNode temp = res;

        while (l1 != null && l2 != null){
            int val1 = l1.val;
            int val2 = l2.val;
            int val = val1>val2?val2:val1;
            temp.next = new ListNode(val);
            temp = temp.next;

            if(l1.val >= l2.val){
                l2 = l2.next;
            }else if(l1.val < l2.val){
                l1 = l1.next;
            }
        }

        if(l1 == null){
            temp.next = l2;
        }

        if(l2 == null){
            temp.next = l1;
        }

        return res.next;

    }
}
