package com.cn.codebrush.链表;

/**
 * @Author Boolean
 * @Date 2022/5/20 21:47
 * @Version 1.0
 */
public class No725分隔链表 {



    public ListNode[] splitListToParts(ListNode head, int k) {

        int len = 0;
        ListNode temp = head;
        while(temp != null){
            temp = temp.next;
            len ++;
        }

        ListNode[] nodes = new ListNode[k];
        int width = len/k;
        int rem = len%k;

        for(int i=0;i<k;i++){
            nodes[i] = head;
            for(int j=1;j<width+(i<rem?1:0);j++){
                head = head.next;
            }

            if(head == null){
                head = null;   //后面没有了  所以赋为空
            }else {
                ListNode node = head.next;
                head.next = null;   //切断与后面的联系，形成一个独立的链
                head = node;
            }
        }

        return nodes;

    }
}
