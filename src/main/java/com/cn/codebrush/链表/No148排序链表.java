package com.cn.codebrush.链表;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @Author Boolean
 * @Date 2022/5/19 17:59
 * @Version 1.0
 */
public class No148排序链表 {

    public ListNode sortList(ListNode head) {
        if(head == null){
            return null;
        }
        List<Integer> list = new ArrayList<>();
        while(head != null){
            list.add(head.val);
            head = head.next;
        }

        Collections.sort(list);
        ListNode res = new ListNode();
        ListNode temp = res;
        for(int i=0;i<list.size();i++){
            temp.next = new ListNode(list.get(i));
            temp = temp.next;
        }
        return res.next;
    }
}
