package com.cn.codebrush.链表;

/**
 * @Author Boolean
 * @Date 2022/5/18 18:21
 * @Version 1.0
 */
public class No剑指OfferII024反转链表 {
    public static void main(String[] args) {

    }

    public ListNode reverseList(ListNode head) {
        if(head == null){
            return null;
        }
        ListNode res = null;
        while(head != null){
            ListNode temp = head.next;
            head.next = res;
            res = head;
            head = temp;
        }

        return res;
    }
}
