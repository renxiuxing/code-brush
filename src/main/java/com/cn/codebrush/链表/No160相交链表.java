package com.cn.codebrush.链表;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author Boolean
 * @Date 2022/6/15 20:32
 * @Version 1.0
 */
public class No160相交链表 {
    public static void main(String[] args) {

    }


    /**
     * 解法二  双指针
     */
    public ListNode getIntersectionNode2(ListNode headA, ListNode headB) {
        if(headA == null || headB == null){
            return null;
        }

        ListNode pA=headA,pB=headB;
        while(pA != pB){
            pA = pA == null?headB:pA.next;
            pB = pB == null?headA:pB.next;
        }

        return pA;
    }


    /**
     * 解法一  使用集合
     * @param headA
     * @param headB
     * @return
     */
    public ListNode getIntersectionNode(ListNode headA, ListNode headB) {
        Map<ListNode,ListNode> map = new HashMap<>();
        while(headA != null){
            map.put(headA,headA);
            headA = headA.next;
        }

        while(headB != null){
            if(map.containsKey(headB)){
                return headB;
            }
            headB = headB.next;
        }

        return null;
    }
}
