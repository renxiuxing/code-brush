package com.cn.codebrush.链表;

import java.util.Deque;
import java.util.LinkedList;

/**
 * @Author Boolean
 * @Date 2022/6/20 10:52
 * @Version 1.0
 */
public class No143重排链表 {
    public static void main(String[] args) {
        ListNode l1 = new ListNode(1);
        ListNode node1 = new ListNode(2);
        ListNode node2 = new ListNode(3);
        ListNode node3 = new ListNode(4);
        ListNode node4 = new ListNode(5);
        l1.next = node1;
        node1.next=node2;
        node2.next=node3;
        node3.next=node4;
        reorderList(l1);
    }

    public static void reorderList(ListNode head) {
        Deque<ListNode> deque = new LinkedList<>();
        while(head != null){
            ListNode  temp =  head.next;
            head.next = null;
            deque.push(head);
            head = temp;
        }

        head = deque.pollLast();
        ListNode temp = head;
        int i = 0;
        while(!deque.isEmpty()){
            if(i % 2 == 0){
                temp.next = deque.pollFirst();
            }else {
                temp.next = deque.pollLast();
            }
            i++;
            temp = temp.next;
        }
    }
}
