package com.cn.codebrush.链表;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author Boolean
 * @Date 2022/6/15 20:23
 * @Version 1.0
 */
public class No142环形链表II {
    public static void main(String[] args) {

    }

    public ListNode detectCycle(ListNode head) {
        Map<ListNode,ListNode> map = new HashMap<>();
        while(head != null){
            if(map.containsKey(head)){
                return head;
            }else {
                map.put(head,head);
            }
            head = head.next;
        }

        return null;
    }
}
