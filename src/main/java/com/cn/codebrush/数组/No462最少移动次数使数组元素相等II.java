package com.cn.codebrush.数组;

import java.util.Arrays;

/**
 * @Author Boolean
 * @Date 2022/5/19 15:30
 * @Version 1.0
 */
public class No462最少移动次数使数组元素相等II {
    public static void main(String[] args) {

    }


    /**
     * 中位数  不是平均数
     * @param nums
     * @return
     */
    public int minMoves2(int[] nums) {
        Arrays.sort(nums);
        int i=0,j=nums.length-1;
        int res = 0;
        while(i<j){
            res += nums[j--] - nums[i++];
        }
        return res;

    }
}
