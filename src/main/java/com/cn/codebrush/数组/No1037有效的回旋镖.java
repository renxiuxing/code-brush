package com.cn.codebrush.数组;

/**
 * @Author Boolean
 * @Date 2022/6/8 22:52
 * @Version 1.0
 */
public class No1037有效的回旋镖 {
    public static void main(String[] args) {
        int[][] points = {{1,1},{1,1},{3,2}};
        int[][] points1 = {{0,0},{0,2},{2,1}};
        /*double k = (double) 1/2;
        System.out.println(k);*/

        System.out.println(isBoomerang(points));
    }

    public static boolean isBoomerang(int[][] points) {
        int x1 = points[0][0] - points[1][0];
        int y1 = points[0][1] - points[1][1];

        int x2 = points[0][0] - points[2][0];
        int y2 = points[0][1] - points[2][1];

        return x1 * y2 != x2 * y1;
    }
}
