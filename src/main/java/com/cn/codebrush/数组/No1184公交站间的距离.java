package com.cn.codebrush.数组;

public class No1184公交站间的距离 {
    public static void main(String[] args) {
        int[] nums = {7,10,1,12,11,14,5,0};

        /*[7,10,1,12,11,14,5,0]
        7
        2*/

        System.out.println(distanceBetweenBusStops(nums,7,2));

    }

    public static int distanceBetweenBusStops(int[] distance, int start, int destination) {
        int n = distance.length;
        int len1 = 0;
        int len2 = 0;
        int left = Math.min(start,destination);
        int right = Math.max(start,destination);
        for(int i=0;i<n;i++){
            if(left<=i && i<right){
                len1 += distance[i];
            }else {
                len2 += distance[i];
            }

        }

        return len1>len2?len2:len1;
    }
}
