package com.cn.codebrush.数组;

import java.util.ArrayList;
import java.util.List;

public class No1260二维网格迁移 {
    public static void main(String[] args) {
        int[][] nums = {{1,2,3},{4,5,6},{7,8,9}};
        System.out.println(shiftGrid(nums,1));
    }

    public static List<List<Integer>> shiftGrid(int[][] grid, int k) {
        int m = grid.length;
        int n = grid[0].length;
        int temp = grid[0][0];
        for(int p =0;p<k;p++){
            for(int i =0;i<m;i++){
                for(int j=0;j<n;j++){
                    if(i == 0 && j == 0){
                        grid[i][j] = grid[m - 1][n - 1];
                    }else {
                        int val = temp;
                        temp = grid[i][j];
                        grid[i][j] = val;
                    }
                }
            }
        }
        List<List<Integer>> list = new ArrayList<>();
        for(int i =0;i<m;i++){
            List sonList = new ArrayList();
            for(int j=0;j<n;j++){
                sonList.add(grid[i][j]);
            }
            list.add(sonList);
        }


        return list;
    }
}
