package com.cn.codebrush.数组;

/**
 * @Author Boolean
 * @Date 2022/4/28 11:29
 * @Version 1.0
 */
public class No905按奇偶排序数组 {
    public static void main(String[] args) {
        int[] nums= {3,2,1,4};
        int[] nums1= {0};
        System.out.println(sortArrayByParity(nums1));
    }


    /**
     * 双指针
     * @param nums
     * @return
     */
    public static int[] sortArrayByParity(int[] nums) {
        int n = nums.length;
        int[] resNums = new int[n];
        int k =0;
        int q =0;
        for(int i=0;i<n;i++){
            if(nums[i]%2 == 0){
                resNums[k]=nums[i];
                k++;
            }else {
                resNums[n-q-1]=nums[i];
                q++;
            }

        }
        return resNums;
    }


    /**
     * 双指针
     * @param nums
     * @return
     */
    public static int[] sortArrayByParity3(int[] nums) {
        int n = nums.length;
        int k =0;
        int q =n-1;
        for(int i=0;i<n;i++){
            if(k<q){
                if(nums[i]%2 == 0){
                    k++;
                }else {
                    int temp = nums[k];
                    //书写方式一
                    /*nums[k] = nums[q];
                    nums[q] = temp;
                    q--;
                    i--;*/
                    //书写方式二
                    nums[k] = nums[q];
                    nums[q--] = temp;
                    i--;
                }
            }
        }
        return nums;
    }

    /**
     * 双指针  精简版
     * @param nums
     * @return
     */
    public static int[] sortArrayByParity2(int[] nums) {
        int n = nums.length;
        for(int i=0,j=n-1;i<j;i++){
            if(nums[i]%2 == 1){
                int temp = nums[j];
                nums[j--] = nums[i];
                nums[i--] = temp;
            }
        }
        return nums;
    }
}
