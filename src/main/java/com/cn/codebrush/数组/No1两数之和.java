package com.cn.codebrush.数组;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author Boolean
 * @Date 2022/5/21 14:07
 * @Version 1.0
 */
public class No1两数之和 {
    public static void main(String[] args) {

    }

    /**
     * 解法一
     * @param nums
     * @param target
     * @return
     */
    public int[] twoSum(int[] nums, int target) {
        int n = nums.length;
        int[] res = new int[2];
        for(int i=0;i<n-1;i++){
            for(int j=i+1;j<n;j++){
                if(nums[i] + nums[j] == target){
                    res[0] = i;
                    res[1] = j;
                    return res;
                }
            }
        }
        return res;
    }

    /**
     * 解法二
     */
    public int[] twoSum2(int[] nums, int target) {
        int n = nums.length;
        int[] res = new int[2];
        Map<Integer,Integer> map = new HashMap<>();
        for(int i=0;i<n;i++){
            if(map.containsKey(nums[i])){
                res[0] = map.get(nums[i]);
                res[1] = i;
            }
            map.put(target-nums[i],i);
        }
        return res;
    }

}
