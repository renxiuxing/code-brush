package com.cn.codebrush.数组;

/**
 * @Author Boolean
 * @Date 2022/6/16 10:30
 * @Version 1.0
 */
public class No55跳跃游戏 {
    public static void main(String[] args) {
        int[] nums = {2,3,1,1,4};
        int[] nums1 = {3,2,1,0,4};
        int[] nums2 = {2,1};
        int[] nums3 = {2,0,0};
        int[] nums4 = {1,1,2,2,0,1,1};
        System.out.println(canJump(nums4));
    }

    /**
     * 贪心算法
     * @param nums
     * @return
     */
    public static boolean canJump(int[] nums) {
        int n = nums.length;
        if(n == 1){
            return true;
        }
        int maxLen = 0;
        for(int i=0;i<n;i++){
            if(i > maxLen){
                break;
            }
            int num = nums[i];
            if(num >= n-i-1){
                return true;
            }else {
                maxLen = maxLen>(i+num)?maxLen:(i+num);
            }
        }

        return false;
    }
}
