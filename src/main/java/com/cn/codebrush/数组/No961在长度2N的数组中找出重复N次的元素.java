package com.cn.codebrush.数组;

import java.util.Arrays;

/**
 * @Author Boolean
 * @Date 2022/5/21 13:54
 * @Version 1.0
 */
public class No961在长度2N的数组中找出重复N次的元素 {
    public static void main(String[] args) {

    }

    public int repeatedNTimes(int[] nums) {
        int len = nums.length;
        int n = len/2;

        Arrays.sort(nums);
        for(int i=1;i<len;i++){
            if(nums[i-1] == nums[i]){
                return nums[i];
            }
        }

        return nums[0];
    }
}
