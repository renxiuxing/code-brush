package com.cn.codebrush.数组;

/**
 * @Author Boolean
 * @Date 2022/5/28 16:41
 * @Version 1.0
 */
public class No27移除元素 {

    public int removeElement(int[] nums, int val) {
        int n = nums.length;
        int slow = 0;
        for(int i=0;i<n;i++){
            if(nums[i] != val){
                nums[slow] = nums[i];
                slow ++;
            }
        }

        return slow;
    }
}
