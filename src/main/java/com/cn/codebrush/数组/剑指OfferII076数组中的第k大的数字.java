package com.cn.codebrush.数组;

/**
 * @Author Boolean
 * @Date 2022/7/4 11:41
 * @Version 1.0
 */
public class 剑指OfferII076数组中的第k大的数字 {


    public int findKthLargest(int[] nums, int k) {
        int n = nums.length;
        for(int i=0;i<k;i++){
            for(int j=i+1;j<n;j++){
                if(nums[i] < nums[j]){
                    int temp = nums[i];
                    nums[i] = nums[j];
                    nums[j] = temp;
                }
            }
        }

        return nums[k-1];
    }
}
