package com.cn.codebrush.数组;

/**
 * @Author Boolean
 * @Date 2022/6/6 22:19
 * @Version 1.0
 */
public class No88合并两个有序数组 {
    public static void main(String[] args) {

    }

    public void merge(int[] nums1, int m, int[] nums2, int n) {
        int k = m+n-1;
        int p = m-1;
        int q = n-1;

        while(p>=0 && q>=0){
            if(nums1[p] >= nums2[q]){
                nums1[k--] = nums1[p--];
            }else {
                nums1[k--] = nums2[q--];
            }
        }

        for(int i=q;i>=0;i--){
            nums1[k--] = nums2[q--];
        }
    }
}
