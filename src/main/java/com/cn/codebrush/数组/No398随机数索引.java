package com.cn.codebrush.数组;

import java.util.*;

/**
 * @Author Boolean
 * @Date 2022/4/25 14:17
 * @Version 1.0
 */
public class No398随机数索引 {
    public static void main(String[] args) {
        int[] nums = new int[] {1,2,3,3,3};
        Solution solution = new Solution(nums);

        // pick(3) 应该返回索引 2,3 或者 4。每个索引的返回概率应该相等。
        //["Solution","pick","pick","pick"]
        //[[[1,2,3,3,3]],[3],[1],[3]]
        System.out.println(solution.pick(3));

    }
}


class Solution {
    Map<Integer, List<Integer>> map = new HashMap<>();
    Random random = new Random();
    public Solution(int[] nums) {
        for(int i=0;i<nums.length;i++){
            map.putIfAbsent(nums[i],new ArrayList<>());
            map.get(nums[i]).add(i);
        }
    }

    public int pick(int target) {
        List<Integer> list =  map.get(target);
        return list.get(random.nextInt(list.size()));
    }
}

/**
 * Your Solution object will be instantiated and called as such:
 * Solution obj = new Solution(nums);
 * int param_1 = obj.pick(target);
 */