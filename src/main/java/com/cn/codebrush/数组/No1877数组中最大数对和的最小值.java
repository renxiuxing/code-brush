package com.cn.codebrush.数组;

import java.util.Arrays;

/**
 * @Author Boolean
 * @Date 2022/6/18 15:47
 * @Version 1.0
 */
public class No1877数组中最大数对和的最小值 {
    public static void main(String[] args) {
        int[] nums = {4,1,5,1,2,5,1,5,5,4};
        System.out.println(minPairSum(nums));
    }


    public static int minPairSum(int[] nums) {
        int n = nums.length;
        int res =0;
        Arrays.sort(nums);
        int left =0,right =n-1;
        while(left<right){
            res = Math.max(res,(nums[left]+nums[right]));
            left++;
            right--;
        }

        return res;
    }
}
