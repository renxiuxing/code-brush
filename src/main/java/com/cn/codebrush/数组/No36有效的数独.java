package com.cn.codebrush.数组;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author Boolean
 * @Date 2022/5/10 14:51
 * @Version 1.0
 */
public class No36有效的数独 {
    public static void main(String[] args) {
        char[][] board = {
                         {'8','3','.','.','7','.','.','.','.'}
                        ,{'6','.','.','1','9','5','.','.','.'}
                        ,{'.','9','8','.','.','.','.','6','.'}
                        ,{'8','.','.','.','6','.','.','.','3'}
                        ,{'4','.','.','8','.','3','.','.','1'}
                        ,{'7','.','.','.','2','.','.','.','6'}
                        ,{'.','6','.','.','.','.','2','8','.'}
                        ,{'.','.','.','4','1','9','.','.','5'}
                        ,{'.','.','.','.','8','.','.','7','9'}
        };

        char[][] board2 = {
                {'.','.','4','.','.','.','6','3','.'},
                {'.','.','.','.','.','.','.','.','.'},
                {'5','.','.','.','.','.','.','9','.'},
                {'.','.','.','5','6','.','.','.','.'},
                {'4','.','3','.','.','.','.','.','1'},
                {'.','.','.','7','.','.','.','.','.'},
                {'.','.','.','5','.','.','.','.','.'},
                {'.','.','.','.','.','.','.','.','.'},
                {'.','.','.','.','.','.','.','.','.'}};


        System.out.println(isValidSudoku(board2));
    }

    public static boolean isValidSudoku(char[][] board) {
        boolean[][] col = new boolean[9][9];
        boolean[][] row = new boolean[9][9];
        boolean[][] block = new boolean[9][9];

        for(int i=0;i<9;i++){
            for(int j=0;j<9;j++){
                if(board[i][j] != '.'){
                    int num = board[i][j] - '1';  //得到相应的 int 类型
                    int blockIndex = i/3*3 + j/3;  //9个小块，分别将 i，j 带入计算一下就可以
                    if(col[i][num] || row[j][num] || block[blockIndex][num]){
                        return false;
                    }else {
                        col[i][num] =true;
                        row[j][num] =true;
                        block[blockIndex][num] =true;
                    }
                }

            }
        }
        return true;
    }
}
