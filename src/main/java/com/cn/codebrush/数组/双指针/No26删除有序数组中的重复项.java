package com.cn.codebrush.数组.双指针;

/**
 * @Author Boolean
 * @Date 2022/6/5 22:59
 * @Version 1.0
 */
public class No26删除有序数组中的重复项 {
    public static void main(String[] args) {

    }

    public int removeDuplicates(int[] nums) {
        int slow = 1;
        for(int i=1;i<nums.length;i++){
            if(nums[i] != nums[i-1]){
                nums[slow] = nums[i];
                slow++;
            }
        }
        return slow;
    }
}
