package com.cn.codebrush.数组;

/**
 * @Author Boolean
 * @Date 2022/6/18 16:53
 * @Version 1.0
 */
public class No134加油站 {
    public static void main(String[] args) {
        int[] gas = {5,1,2,3,4};
        int[] cost = {4,4,1,5,1};
        System.out.println(canCompleteCircuit(gas,cost));
    }

    public static int canCompleteCircuit(int[] gas, int[] cost) {
        int n = gas.length;
        int index = 0;
        int curGas = 0; //当前量
        int total = 0; //剩余量
        for(int i=0;i<n;i++){
            curGas += gas[i] - cost[i];
            total +=  gas[i] - cost[i];
            if(curGas < 0){
                index = i+1;
                curGas =0;
            }
        }

        if(total < 0){
            return -1;
        }else {
            return index;
        }

    }
}
