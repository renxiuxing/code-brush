package com.cn.codebrush.数组;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @Author Boolean
 * @Date 2022/7/4 10:43
 * @Version 1.0
 */
public class No1200最小绝对差 {
    public static void main(String[] args) {
        int[] arr = {40,11,26,27,-20};
        System.out.println(minimumAbsDifference(arr));
    }

    /*输入：arr = [40,11,26,27,-20]
    输出：[[26,27]]
    */
    public static List<List<Integer>> minimumAbsDifference(int[] arr) {
        Arrays.sort(arr);
        List res = new ArrayList();
        int min = Integer.MAX_VALUE;
        for(int i=1;i<arr.length;i++){
            if(arr[i]-arr[i-1]<=min){
                if(arr[i]-arr[i-1]<min){
                    min = arr[i]-arr[i-1];
                    res.clear();
                }
                res.add(Arrays.asList(arr[i-1],arr[i]));
            }
        }

        return res;
    }
}
