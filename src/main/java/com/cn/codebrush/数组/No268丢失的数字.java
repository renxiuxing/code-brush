package com.cn.codebrush.数组;

import java.util.Arrays;

/**
 * @Author Boolean
 * @Date 2022/5/16 21:14
 * @Version 1.0
 */
public class No268丢失的数字 {
    public static void main(String[] args) {
        int[] nums1= {3,0,1};
        System.out.println(missingNumber(nums1));
    }

    public static int missingNumber(int[] nums) {
        int n = nums.length;
        Arrays.sort(nums);
        for(int i=0;i<n;i++){
            if(nums[i] != i ){
                return i;
            }
        }
        return n;
    }
}
