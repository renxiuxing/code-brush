package com.cn.codebrush.数组;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @Author Boolean
 * @Date 2022/5/9 16:03
 * @Version 1.0
 */
public class No31下一个排列 {
    public static void main(String[] args) {
        /**
         * 31. 下一个排列
         * 题干的意思是：找出这个数组排序出的所有数中，刚好比当前数大的那个数
         * 比如当前 nums = [1,2,3]。这个数是123，找出1，2，3这3个数字排序可能的所有数，排序后，比123大的紧挨着的那个数 也就是132
         * 如果当前 nums = [3,2,1]。这就是1，2，3所有排序中最大的那个数，那么就返回1，2，3排序后所有数中最小的那个，也就是1，2，3 -> [1,2,3]
         */

        int[] nums = {1,2,3};
        int[] nums1 = {3,2,1};
        int[] nums2 = {1,3,2};
        nextPermutation2(nums2);

    }

    public static void nextPermutation2(int[] nums) {
        int n = nums.length;
        for(int i=n-1;i>=0;i--){
            if(nums[i]>nums[i-1]){
                Arrays.sort(nums,i,n);
            }
            for(int j=i;j<n;j++){
                if(nums[j] > nums[i-1]){
                    int temp = nums[j];
                    nums[j] = nums[i-1];
                    nums[i-1] = temp;
                    return;
                }
            }
        }
        Arrays.sort(nums);
    }



    /**
     * 64 / 265 个通过测试用例
     * 最后执行的输入：
     * [6,7,5,3,5,6,2,9,1,2,7,0,9]
     */
    static int checkNum = Integer.MAX_VALUE;
    public static void nextPermutation(int[] nums) {
        int[] visited = new int[nums.length];
        String s = "";
        for(int i=0;i<nums.length;i++){
            s=s+nums[i];
        }
        int sumNum = Integer.valueOf(s);
        nextPermutation(nums,new ArrayList(),visited,sumNum);

        if(checkNum == Integer.MAX_VALUE){
            int[] temp = nums.clone();
            int o = nums.length;
            for(int q=0;q<nums.length;q++){
                nums[q] = temp[o-1];
                o--;
            }
        }else{
            for(int p=nums.length-1;p>=0;p--){
                nums[p] = checkNum % 10;
                checkNum = checkNum / 10;
            }
        }
    }

    public static void nextPermutation(int[] nums, List list,int[] visited,int sumNum) {
        if(list.size() == nums.length){
            String s = "";
            for(int i=0;i<list.size();i++){
                s=s+list.get(i);
            }
            int sumList = Integer.valueOf(s);

            if(sumList>sumNum && sumList<checkNum){
                checkNum = sumList;
            }
        }

        for(int i=0;i<nums.length;i++){
            if(visited[i] == 1){continue;}
            visited[i] = 1;
            list.add(nums[i]);
            nextPermutation(nums,list,visited,sumNum);
            visited[i] = 0;
            list.remove(list.size()-1);
        }
    }




}
