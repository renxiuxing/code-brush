package com.cn.codebrush.数组;

/**
 * @Author Boolean
 * @Date 2022/7/12 11:16
 * @Version 1.0
 */
public class No215数组中的第K个最大元素 {
    public static void main(String[] args) {
        int[] nums = {1,8,4,5,6,8,9};
        System.out.println(findKthLargest(nums,3));
    }

    public static int findKthLargest(int[] nums, int k) {
        int n= nums.length;
        for(int i=0;i<k;i++){
            int max = i;
            for(int j=i+1;j<n;j++){
                if(nums[j]>nums[max]){
                    max=j;
                }
            }
            if(i != max){
                int temp = nums[i];
                nums[i] = nums[max];
                nums[max] = temp;
            }
        }

        return nums[k-1];
    }

}
