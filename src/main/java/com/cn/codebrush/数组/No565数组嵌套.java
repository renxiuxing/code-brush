package com.cn.codebrush.数组;

public class No565数组嵌套 {
    public static void main(String[] args) {

    }

    //565. 数组嵌套
    public static int arrayNesting(int[] nums) {

        //dp[i]表示以i结尾的元素的嵌套最大长度
        int ret = 0;
        int len = nums.length;
        for(int i = 0; i < len; i++){
            if(nums[i] == -1) continue;
            int last_index, index = i, k = 0;
            while(nums[index] != -1){
                last_index = index;
                index = nums[index];
                nums[last_index] = -1; // 将当前环上的元素设置为访问过
                ++k;
            }
            ret = Math.max(ret, k);
        }
        return ret;


        /*int n = nums.length;
        int res = 0;
        for(int i=0;i<n;i++){
            int val = nums[i];
            Map<Integer,Integer> map = new HashMap();
            while(!map.containsKey(val)){
                map.put(val,nums[val]);
                val = nums[val];
            }
            res = Math.max(res,map.size());
        }

        return res;*/
    }
}
