package com.cn.codebrush.数组.贪心算法;

import java.util.Arrays;

public class No455分发饼干 {
    public static void main(String[] args) {

    }

    public int findContentChildren(int[] g, int[] s) {
        Arrays.sort(g);
        Arrays.sort(s);
        int c = 0;
        int res = 0;
        for(int i =0;i<s.length;i++){
            if(c >= g.length){
                break;
            }
            if(s[i] >= g[c]){
                res ++;
                c++;
            }
        }

        return res;
    }
}
