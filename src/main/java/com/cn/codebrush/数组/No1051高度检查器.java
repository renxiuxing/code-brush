package com.cn.codebrush.数组;

import java.util.Arrays;

/**
 * @Author Boolean
 * @Date 2022/6/13 11:13
 * @Version 1.0
 */
public class No1051高度检查器 {
    public static void main(String[] args) {

    }

    public int heightChecker(int[] heights) {
        int[] temp = Arrays.copyOf(heights,heights.length);
        Arrays.sort(temp);
        int count = 0;
        for(int i=0;i<heights.length;i++){
            if(heights[i] != temp[i]){
                count++;
            }
        }
        return count;
    }
}
