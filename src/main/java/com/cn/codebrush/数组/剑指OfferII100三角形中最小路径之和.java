package com.cn.codebrush.数组;

import java.util.ArrayList;
import java.util.List;

public class 剑指OfferII100三角形中最小路径之和 {
    public static void main(String[] args) {
        //triangle = [[2],[3,4],[6,5,7],[4,1,8,3]]
        List list1 = new ArrayList();
        list1.add(2);
        List list2 = new ArrayList();
        list2.add(3);
        list2.add(4);
        List list3 = new ArrayList();
        list3.add(6);
        list3.add(5);
        list3.add(7);
        List list4 = new ArrayList();
        list4.add(4);
        list4.add(1);
        list4.add(8);
        list4.add(3);
        List triangle = new ArrayList();
        triangle.add(list1);
        triangle.add(list2);
        triangle.add(list3);
        triangle.add(list4);

        System.out.println(minimumTotal(triangle));
    }

    public static int minimumTotal(List<List<Integer>> triangle) {
        int m = triangle.size();
        int n = triangle.get(m-1).size();
        int[][] dp = new int[m][n];
        dp[0][0] = triangle.get(0).get(0);
        for(int i =1;i<n;i++){
            List<Integer> list = triangle.get(0);
            for(int j =0;j<list.size();j++){
                if(j == 0){
                    dp[i][j] = dp[i-1][j] + list.get(j);
                }else if(j >= triangle.get(i-1).size()){
                    dp[i][j] = dp[i-1][j-1] + list.get(j);
                }else{
                    dp[i][j] = Math.min(dp[i-1][j],dp[i-1][j-1]) + list.get(j);
                }

            }
        }

        int res =0;
        for(int val:dp[m-1]){
            res = Math.min(res,val);
        }
        return res;
    }
}
