package com.cn.codebrush.数组;

import java.util.Arrays;

/**
 * @Author Boolean
 * @Date 2022/6/18 16:21
 * @Version 1.0
 */
public class No1561你可以获得的最大硬币数目 {
    public static void main(String[] args) {

    }

    public int maxCoins(int[] piles) {
        int n = piles.length;
        Arrays.sort(piles);
        int left =0,right =n-1;
        int res =0;
        while(left < right){
            res = res + piles[right-1];
            left++;
            right = right-2;
        }

        return res;
    }
}
