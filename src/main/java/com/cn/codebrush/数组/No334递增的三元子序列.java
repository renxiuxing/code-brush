package com.cn.codebrush.数组;

public class No334递增的三元子序列 {
    public static void main(String[] args) {
        int[] nums = {1,2,3,4,5};
        int[] nums1 = {20,1,10,12,5,13};
        int[] nums2 = {1,5,0,4,1,3};
        int[] nums3 = {4,5,2147483647,1,2};

        System.out.println(increasingTriplet(nums1));
    }


    /**
     * 贪心算法
     * @param nums
     * @return
     */
    public static boolean increasingTriplet1(int[] nums) {
        int n = nums.length;
        if (n < 3) {
            return false;
        }
        int first = nums[0], second = Integer.MAX_VALUE;
        for (int i = 1; i < n; i++) {
            int num = nums[i];
            if (num > second) {
                return true;
            } else if (num > first) {
                second = num;
            } else {
                first = num;
            }
        }
        return false;
    }


    public static boolean increasingTriplet(int[] nums) {
        int n = nums.length;
        if (n < 3) {
            return false;
        }
        int minNum = nums[0];
        int secondNum = Integer.MAX_VALUE;
        for(int i=1;i<n;i++){
            if(secondNum < nums[i]){
                return true;
            }else if(nums[i] > minNum){
                secondNum = nums[i];
            }else{
                minNum = nums[i];
            }
        }

        return false;
    }
}
