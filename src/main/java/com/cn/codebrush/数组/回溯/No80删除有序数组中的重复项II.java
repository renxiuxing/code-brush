package com.cn.codebrush.数组.回溯;

import java.util.Arrays;

/**
 * @Author Boolean
 * @Date 2022/5/17 19:42
 * @Version 1.0
 */
public class No80删除有序数组中的重复项II {
    public static void main(String[] args) {
        /**
         * 测试用例：
         * [1,1,1,2,2,3,3,3,3]
         * [1,1,1,2,2,2,3,3,3,3]
         * [1]
         * [1,1,1,1]
         * [1,1,1,2]
         */
        int[] nums = {0,0,1,1,1,1,2,3,3};
        int[] nums1 = {0,0,0,1,1,1,1,2,3,3,3,3};
        int[] nums2 = {1,1,1,2,2,3};
        System.out.println(removeDuplicates(nums1));
    }

    static int len = 0;
    public static int removeDuplicates(int[] nums) {
        int n = nums.length;
        nums = removeDuplicatesHelper(nums,n-1);

        return len+1;
    }

    public static int[] removeDuplicatesHelper(int[] nums,int end) {
        Arrays.sort(nums,0,end+1);
        len = end;
        int count = 1;
        for(int i=1;i<=end;i++){
            if(nums[i-1] == nums[i]){
                if(count == 2){
                    int temp = nums[i];
                    nums[i] = nums[end];
                    nums[end] = temp;
                    return removeDuplicatesHelper(nums,end-1);
                }else {
                    count++;
                }
            }else {
                count = 1;
            }
        }

        return nums;
    }
}
