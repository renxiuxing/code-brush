package com.cn.codebrush.数组.回溯;

import java.util.*;

/**
 * @Author Boolean
 * @Date 2022/4/26 10:16
 * @Version 1.0
 */
public class No90子集II {
    public static void main(String[] args) {
        //给你一个整数数组 nums ，其中可能包含重复元素，请你返回该数组所有可能的子集（幂集）。
        // *****   解集 不能 包含重复的子集。返回的解集中，子集可以按 任意顺序 排列。
        //说明：解集不能包含重复的子集。
        //两个子集如果只是元素的排列顺序不一样，则认为重复
        int[] nums = {1,2,2}; //[[],[1],[1,2],[1,2,2],[2],[2,2]]
        int[] nums1 = {4,4,4,1,4};  //[[],[1],[1,4],[1,4,4],[1,4,4,4],[1,4,4,4,4],[4],[4,4],[4,4,4],[4,4,4,4]]
        System.out.println(subsetsWithDup(nums1));
    }


    /**
     * 回溯法
     * AC
     */
    static Set resList = new HashSet();
    public static List<List<Integer>> subsetsWithDup(int[] nums) {
        Arrays.sort(nums);
        subsetsWithDupHelper(nums,0,new ArrayList());
        resList.add(new ArrayList<>());
        return new ArrayList<>(resList);
    }

    public static void subsetsWithDupHelper(int[] nums,int n,List list) {
        if(list.size() < nums.length && list.size() != 0){
            resList.add(new ArrayList<>(list));
        }else if(list.size() == nums.length){
            resList.add(new ArrayList<>(list));
            return;
        }

        for(int i=n;i<nums.length;i++){
            list.add(nums[i]);
            subsetsWithDupHelper(nums,i+1,list);
            list.remove(list.size()-1);
        }
    }
}
