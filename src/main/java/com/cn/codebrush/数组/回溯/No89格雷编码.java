package com.cn.codebrush.数组.回溯;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author Boolean
 * @Date 2022/7/12 10:46
 * @Version 1.0
 */
public class No89格雷编码 {

    public static void main(String[] args) {
        System.out.println(grayCode(4));  // 16
        //System.out.println(1^3);
    }


    //89
    public static List<Integer> grayCode1(int n) {
        /**
         关键是搞清楚格雷编码的生成过程, G(i) = i ^ (i/2);
         如 n = 3:
         G(0) = 000,
         G(1) = 1 ^ 0 = 001 ^ 000 = 001
         G(2) = 2 ^ 1 = 010 ^ 001 = 011
         G(3) = 3 ^ 1 = 011 ^ 001 = 010
         G(4) = 4 ^ 2 = 100 ^ 010 = 110
         G(5) = 5 ^ 2 = 101 ^ 010 = 111
         G(6) = 6 ^ 3 = 110 ^ 011 = 101
         G(7) = 7 ^ 3 = 111 ^ 011 = 100
         **/
        List<Integer> ret = new ArrayList<>();
        for(int i = 0; i < 1<<n; ++i)
            ret.add(i ^ i>>1);
        return ret;
    }
    /**
     * 找规律：
     *
     * 当 n == 1 ，返回 [0,1] .
     * 当 n == 2 ，返回 [0,1,3,2] .
     * 当 n == 3 ，返回 [0,1,3,2,6,7,5,4] .
     * 当 n == …… ，返回 [0,1,3,2,6,7,5,4,……] .
     * @param n
     * @return
     */
    static List<Integer> list = new ArrayList();
    public static List<Integer> grayCode(int n) {
        list.add(0);
        grayCodeHelper(n,list);
        return list;
    }

    private static void grayCodeHelper(int n, List<Integer> list) {
        int len = list.size();
        if(len == 1 << n){
            return;
        }
        for(int i=len-1;i>=0;i--){
            list.add(list.get(i)+len);
        }
        grayCodeHelper(n,list);
    }
}
