package com.cn.codebrush.数组.回溯;

import java.util.ArrayList;
import java.util.List;

public class No78子集 {
    public static void main(String[] args) {
        int[] nums = {1,2,3}; //[[1], [1, 2], [1, 2, 3], [1, 3], [2], [2, 3], [3], []]
        System.out.println(subsets(nums));
    }


    /**
     * 回溯法
     * AC
     */
    static List resList = new ArrayList();
    public static List<List<Integer>> subsets(int[] nums) {
        subsetsHelper(nums,0,new ArrayList());
        resList.add(new ArrayList<>());
        return resList;
    }

    public static void subsetsHelper(int[] nums,int n,List list) {
        if(list.size() < nums.length && list.size() != 0){
            resList.add(new ArrayList<>(list));
        }else if(list.size() == nums.length){
            resList.add(new ArrayList<>(list));
            return;
        }

        for(int i=n;i<nums.length;i++){
            list.add(nums[i]);
            subsetsHelper(nums,i+1,list);
            list.remove(list.size()-1);
        }
    }
}
