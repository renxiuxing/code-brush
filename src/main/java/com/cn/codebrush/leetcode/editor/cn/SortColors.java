package com.cn.codebrush.leetcode.editor.cn;
// 2022-09-14 16:59:53
// 75  颜色分类
//给定一个包含红色、白色和蓝色、共 n 个元素的数组 nums ，原地对它们进行排序，使得相同颜色的元素相邻，并按照红色、白色、蓝色顺序排列。 
//
// 我们使用整数 0、 1 和 2 分别表示红色、白色和蓝色。 
//
// 
// 
//
// 必须在不使用库的sort函数的情况下解决这个问题。 
//
// 
//
// 示例 1： 
//
// 
//输入：nums = [2,0,2,1,1,0]
//输出：[0,0,1,1,2,2]
// 
//
// 示例 2： 
//
// 
//输入：nums = [2,0,1]
//输出：[0,1,2]
// 
//
// 
//
// 提示： 
//
// 
// n == nums.length 
// 1 <= n <= 300 
// nums[i] 为 0、1 或 2 
// 
//
// 
//
// 进阶： 
//
// 
// 你可以不使用代码库中的排序函数来解决这道题吗？ 
// 你能想出一个仅使用常数空间的一趟扫描算法吗？ 
// 
// Related Topics 数组 双指针 排序 
// 👍 1392 👎 0


public class SortColors{

public static void main(String[] args) {

Solution solution = new SortColors().new Solution();
int[] nums = {2,0,2,1,1,0};
    solution.sortColors(nums);
}

//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    public void sortColors(int[] nums) {
        //三个指针num1、num2、num3将数组nums分成了3个分区，从左往右依次存储0、1、2。 三个指针分别指向各自分区的尾部。
        // 从左到右遍历数组nums，(1)如果nums[i]=0,则1、2区都后移一个位置，给新来的0腾地方。 (2)如果是nums[i]=1，
        // 同样，2区后移一个位置，给新来的1腾地方。前面的0区无影响。
        int num0 = 0, num1 = 0, num2 = 0;
        for(int i = 0; i < nums.length; i++) {
            if(nums[i] == 0) {
                nums[num2++] = 2;
                nums[num1++] = 1;
                nums[num0++] = 0;
            }else if(nums[i] == 1) {
                nums[num2++] = 2;
                nums[num1++] = 1;
            }else {
                nums[num2++] = 2;
            }
        }

    }
}
//leetcode submit region end(Prohibit modification and deletion)


}