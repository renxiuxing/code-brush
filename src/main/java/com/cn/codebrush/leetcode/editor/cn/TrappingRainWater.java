   package com.cn.codebrush.leetcode.editor.cn;

//给定 n 个非负整数表示每个宽度为 1 的柱子的高度图，计算按此排列的柱子，下雨之后能接多少雨水。 
//
// 
//
// 示例 1： 
//
// 
//
// 
//输入：height = [0,1,0,2,1,0,1,3,2,1,2,1]
//输出：6
//解释：上面是由数组 [0,1,0,2,1,0,1,3,2,1,2,1] 表示的高度图，在这种情况下，可以接 6 个单位的雨水（蓝色部分表示雨水）。 
// 
//
// 示例 2： 
//
// 
//输入：height = [4,2,0,3,2,5]
//输出：9
// 
//
// 
//
// 提示： 
//
// 
// n == height.length 
// 1 <= n <= 2 * 104 
// 0 <= height[i] <= 105 
// 
// Related Topics 栈 数组 双指针 动态规划 单调栈 
// 👍 3640 👎 0


public class TrappingRainWater{

public static void main(String[] args) {

Solution solution = new TrappingRainWater().new Solution();

}

//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    public int trap(int[] height) {
        int n = height.length;
        int left = 0;
        int right = n-1;
        int leftMax = 0;
        int rightMax = 0;
        int res = 0;

        while(left<right){
            if(height[left] > height[right]){
                rightMax = Math.max(rightMax,height[right]);
                res += rightMax-height[right];
                right--;
            }else {
                leftMax = Math.max(leftMax,height[left]);
                res += leftMax-height[left];
                left++;
            }
        }

        return res;
    }
}
//leetcode submit region end(Prohibit modification and deletion)


}