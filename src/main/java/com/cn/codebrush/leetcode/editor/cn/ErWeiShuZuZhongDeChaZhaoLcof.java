package com.cn.codebrush.leetcode.editor.cn;

//在一个 n * m 的二维数组中，每一行都按照从左到右递增的顺序排序，每一列都按照从上到下递增的顺序排序。请完成一个高效的函数，输入这样的一个二维数组和一个
//整数，判断数组中是否含有该整数。 
//
// 
//
// 示例: 
//
// 现有矩阵 matrix 如下： 
//
// 
//[
//  [1,   4,  7, 11, 15],
//  [2,   5,  8, 12, 19],
//  [3,   6,  9, 16, 22],
//  [10, 13, 14, 17, 24],
//  [18, 21, 23, 26, 30]
//]
// 
//
// 给定 target = 5，返回 true。 
//
// 给定 target = 20，返回 false。 
//
// 
//
// 限制： 
//
// 0 <= n <= 1000 
//
// 0 <= m <= 1000 
//
// 
//
// 注意：本题与主站 240 题相同：https://leetcode-cn.com/problems/search-a-2d-matrix-ii/ 
// Related Topics 数组 二分查找 分治 矩阵 
// 👍 740 👎 0


public class ErWeiShuZuZhongDeChaZhaoLcof{

public static void main(String[] args) {

Solution solution = new ErWeiShuZuZhongDeChaZhaoLcof().new Solution();
int[][] matrix = {{1,   4,  7, 11, 15}, {2,   5,  8, 12, 19}, {3,   6,  9, 16, 22}, {10, 13, 14, 17, 24}, {18, 21, 23, 26, 30}};
System.out.println(solution.findNumberIn2DArray(matrix,20));


}

//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    public boolean findNumberIn2DArray(int[][] matrix, int target) {
        int n = matrix.length;
        for(int i=0;i<n;i++){
            if(findNumberIn2DArrayHelper(matrix[i],target)){
                return true;
            }
        }

        return false;
    }

    public boolean findNumberIn2DArrayHelper(int[] nums, int target) {
        int n = nums.length;
        int left = 0;
        int right = n;
        while(left<right){
            int temp = (right+left)/2;
            if(nums[temp] == target){
                return true;
            }else if(target > nums[temp]){
                left = temp+1;
            }else {
                right = temp;
            }
        }

        return false;
    }
}
//leetcode submit region end(Prohibit modification and deletion)


}