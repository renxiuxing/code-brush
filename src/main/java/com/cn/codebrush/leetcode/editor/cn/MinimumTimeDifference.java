package com.cn.codebrush.leetcode.editor.cn;

//给定一个 24 小时制（小时:分钟 "HH:MM"）的时间列表，找出列表中任意两个时间的最小时间差并以分钟数表示。 
//
// 
//
// 示例 1： 
//
// 
//输入：timePoints = ["23:59","00:00"]
//输出：1
// 
//
// 示例 2： 
//
// 
//输入：timePoints = ["00:00","23:59","00:00"]
//输出：0
// 
//
// 
//
// 提示： 
//
// 
// 2 <= timePoints.length <= 2 * 104 
// timePoints[i] 格式为 "HH:MM" 
// 
// Related Topics 数组 数学 字符串 排序 
// 👍 210 👎 0


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MinimumTimeDifference{

public static void main(String[] args) {

Solution solution = new MinimumTimeDifference().new Solution();
    List<String> timePoints = new ArrayList<>();
    timePoints.add("12:12");
    timePoints.add("00:13");
    //timePoints.add("04:00");
    //["00:00","04:00","22:00"]    120
    //["12:12","00:13"]   719
System.out.println(solution.findMinDifference(timePoints));
}

//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    public int findMinDifference(List<String> timePoints) {
        int n = timePoints.size();
        int[][] arrs = new int[n][2];
        for(int i=0;i<n;i++){
            String[] strs = timePoints.get(i).split(":");
            int[] arr = new int[2];
            arr[0] = Integer.valueOf(strs[0]);
            arr[1] = Integer.valueOf(strs[1]);
            arrs[i] = arr;
        }
        Arrays.sort(arrs, (a, b) -> a[0] - b[0]);
        int res = Integer.MAX_VALUE;
        for(int j=1;j<n;j++){
            int temp = Math.min(((arrs[j][0] - arrs[j-1][0])*60+(arrs[j][1] - arrs[j-1][1])),(60*24-((arrs[j][0])*60+(arrs[j][1]))));
            res = Math.min (res,temp);
        }

        return res;
    }
}
//leetcode submit region end(Prohibit modification and deletion)


}