package com.cn.codebrush.leetcode.editor.cn;
// 2022-12-05 22:07:43
// 131  分割回文串
//给你一个字符串 s，请你将 s 分割成一些子串，使每个子串都是 回文串 。返回 s 所有可能的分割方案。 
//
// 回文串 是正着读和反着读都一样的字符串。 
//
// 
//
// 示例 1： 
//
// 
//输入：s = "aab"
//输出：[["a","a","b"],["aa","b"]]
// 
//
// 示例 2： 
//
// 
//输入：s = "a"
//输出：[["a"]]
// 
//
// 
//
// 提示： 
//
// 
// 1 <= s.length <= 16 
// s 仅由小写英文字母组成 
// 
// Related Topics 字符串 动态规划 回溯 
// 👍 1326 👎 0


import java.util.ArrayList;
import java.util.List;

public class PalindromePartitioning{

public static void main(String[] args) {

Solution solution = new PalindromePartitioning().new Solution();
    System.out.println(solution.partition("aab"));
}

//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    // 可以使用回溯的方式来解决这个问题，
    // 从前往后进行遍历，逐一的截取字符串进行判断是不是回文，
    // 如果是就将当前字符串记录下来，然后 start+1 进行判断后面的字符串
    // 当遍历到末尾时，就返回
    List<List<String>> resList = new ArrayList<>();
    public List<List<String>> partition(String s) {
        partitionHelper(s,0,new ArrayList());
        return resList;
    }
    public void partitionHelper(String s,int start,List list) {
        if(start == s.length()){
            resList.add(new ArrayList<>(list));
            return;
        }
        for(int i=start;i<s.length();i++){
            String temp = s.substring(start,i+1);
            if(check(temp)){
                list.add(temp);
                partitionHelper(s,i+1,list);
                list.remove(list.size()-1);
            }
        }

    }

    public boolean check(String s) {
        int left = 0;
        int right = s.length()-1;
        while (left<right){
            if(s.charAt(left) != s.charAt(right)){
                return false;
            }

            left++;
            right--;
        }

        return true;
    }
}
//leetcode submit region end(Prohibit modification and deletion)


}