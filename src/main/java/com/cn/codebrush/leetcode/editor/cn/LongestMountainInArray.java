package com.cn.codebrush.leetcode.editor.cn;
// 2022-09-19 10:29:16
// 845  数组中的最长山脉
//把符合下列属性的数组 arr 称为 山脉数组 ： 
//
// 
// arr.length >= 3 
// 存在下标 i（0 < i < arr.length - 1），满足
// 
// arr[0] < arr[1] < ... < arr[i - 1] < arr[i] 
// arr[i] > arr[i + 1] > ... > arr[arr.length - 1] 
// 
// 
// 
//
// 给出一个整数数组 arr，返回最长山脉子数组的长度。如果不存在山脉子数组，返回 0 。 
//
// 
//
// 示例 1： 
//
// 
//输入：arr = [2,1,4,7,3,2,5]
//输出：5
//解释：最长的山脉子数组是 [1,4,7,3,2]，长度为 5。
// 
//
// 示例 2： 
//
// 
//输入：arr = [2,2,2]
//输出：0
//解释：不存在山脉子数组。
// 
//
// 
//
// 提示： 
//
// 
// 1 <= arr.length <= 104 
// 0 <= arr[i] <= 104 
// 
//
// 
//
// 进阶： 
//
// 
// 你可以仅用一趟扫描解决此问题吗？ 
// 你可以用 O(1) 空间解决此问题吗？ 
// 
// Related Topics 数组 双指针 动态规划 枚举 
// 👍 246 👎 0


public class LongestMountainInArray{

public static void main(String[] args) {

Solution solution = new LongestMountainInArray().new Solution();
int[] arr = {0,1,2,3,4,5,4,3,2,1,0};
    System.out.println(solution.longestMountain(arr));
}

//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    public int longestMountain(int[] arr) {
        int n = arr.length;
        int res = 0;
        for(int i=1;i<n-1;i++){
            int left = i;
            int right = i;
            while (left>=1 && arr[left]>arr[left-1]){
                left--;
            }
            while (right<=n-2 && arr[right]>arr[right+1]){
                right++;
            }

            if(left!=i && right!=i){
                res = Math.max(res,(right-left+1));
            }

        }

        return res<3?0:res;
    }



















}
//leetcode submit region end(Prohibit modification and deletion)


}