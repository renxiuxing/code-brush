package com.cn.codebrush.leetcode.editor.cn;

//给你一个整数数组 nums ，数组中的元素 互不相同 。返回该数组所有可能的子集（幂集）。 
//
// 解集 不能 包含重复的子集。你可以按 任意顺序 返回解集。 
//
// 
//
// 示例 1： 
//
// 
//输入：nums = [1,2,3]
//输出：[[],[1],[2],[1,2],[3],[1,3],[2,3],[1,2,3]]
// 
//
// 示例 2： 
//
// 
//输入：nums = [0]
//输出：[[],[0]]
// 
//
// 
//
// 提示： 
//
// 
// 1 <= nums.length <= 10 
// -10 <= nums[i] <= 10 
// nums 中的所有元素 互不相同 
// 
// Related Topics 位运算 数组 回溯 
// 👍 1723 👎 0


import java.util.ArrayList;
import java.util.List;

public class Subsets{

public static void main(String[] args) {

Solution solution = new Subsets().new Solution();
    int[] nums = {1,2,3};
    System.out.println(solution.subsets(nums));
}

//leetcode submit region begin(Prohibit modification and deletion)
class Solution {

    List<List<Integer>> resList = new ArrayList<>();
    public List<List<Integer>> subsets(int[] nums) {
        subsetsHelper(nums,new ArrayList(),0);
        return resList;
    }

    public void subsetsHelper(int[] nums,List list,int k) {
        if(list.size() <= nums.length){
            resList.add(new ArrayList<>(list));
        }else {
            return;
        }

        for(int i=k;i<nums.length;i++){
            list.add(nums[i]);
            subsetsHelper(nums,list,i+1);
            list.remove(list.size()-1);
        }
    }




    /*List<List<Integer>>  resList = new ArrayList();
    public List<List<Integer>> subsets(int[] nums) {
        int n = nums.length;
        subsetsHelper(nums,0,new ArrayList<Integer>());
        return resList;
    }

    public void subsetsHelper(int[] nums,int start,List<Integer> list) {
        if(start <= nums.length){
            resList.add(new ArrayList<Integer>(list));
        }
        if(start > nums.length){
            return;
        }

        for(int i=start;i<nums.length;i++){
            list.add(nums[i]);
            subsetsHelper(nums,i+1,list);
            list.remove(list.size()-1);
        }
    }*/
}
//leetcode submit region end(Prohibit modification and deletion)


}