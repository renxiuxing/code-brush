package com.cn.codebrush.leetcode.editor.cn;
// 2022-09-21 16:46:59
// 102  二叉树的层序遍历
//给你二叉树的根节点 root ，返回其节点值的 层序遍历 。 （即逐层地，从左到右访问所有节点）。 
//
// 
//
// 示例 1： 
//
// 
//输入：root = [3,9,20,null,null,15,7]
//输出：[[3],[9,20],[15,7]]
// 
//
// 示例 2： 
//
// 
//输入：root = [1]
//输出：[[1]]
// 
//
// 示例 3： 
//
// 
//输入：root = []
//输出：[]
// 
//
// 
//
// 提示： 
//
// 
// 树中节点数目在范围 [0, 2000] 内 
// -1000 <= Node.val <= 1000 
// 
// Related Topics 树 广度优先搜索 二叉树 
// 👍 1465 👎 0


import java.util.*;

public class BinaryTreeLevelOrderTraversal{

public static void main(String[] args) {

Solution solution = new BinaryTreeLevelOrderTraversal().new Solution();

}

//leetcode submit region begin(Prohibit modification and deletion)

  //Definition for a binary tree node.
  public class TreeNode {
      int val;
      TreeNode left;
      TreeNode right;
      TreeNode() {}
     TreeNode(int val) { this.val = val; }
      TreeNode(int val, TreeNode left, TreeNode right) {
          this.val = val;
          this.left = left;
          this.right = right;
      }
  }
class Solution {
    public List<List<Integer>> levelOrder(TreeNode root) {
        Deque<TreeNode> deque = new LinkedList();
        List<List<Integer>> resList = new ArrayList<>();
        if(root == null){return resList;}
        deque.add(root);
        while(!deque.isEmpty()){
            int k = deque.size();
            List<Integer> list = new ArrayList<>();
            while(k>0){
                if(deque.peek().left != null){
                    deque.add(deque.peek().left);
                }
                if(deque.peek().right != null){
                    deque.add(deque.peek().right);
                }
                list.add(deque.poll().val);
                k--;
            }

            resList.add(list);
        }

        return resList;
    }


























}
//leetcode submit region end(Prohibit modification and deletion)


}