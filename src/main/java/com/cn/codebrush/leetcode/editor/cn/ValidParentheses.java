package com.cn.codebrush.leetcode.editor.cn;
// 2023-06-09 10:02:13
// 20  有效的括号
//给定一个只包括 '('，')'，'{'，'}'，'['，']' 的字符串 s ，判断字符串是否有效。 
//
// 有效字符串需满足： 
//
// 
// 左括号必须用相同类型的右括号闭合。 
// 左括号必须以正确的顺序闭合。 
// 每个右括号都有一个对应的相同类型的左括号。 
// 
//
// 
//
// 示例 1： 
//
// 
//输入：s = "()"
//输出：true
// 
//
// 示例 2： 
//
// 
//输入：s = "()[]{}"
//输出：true
// 
//
// 示例 3： 
//
// 
//输入：s = "(]"
//输出：false
// 
//
// 
//
// 提示： 
//
// 
// 1 <= s.length <= 104 
// s 仅由括号 '()[]{}' 组成 
// 
// Related Topics 栈 字符串 
// 👍 3967 👎 0


import sun.plugin.javascript.navig.Link;

import java.util.Deque;
import java.util.LinkedList;

public class ValidParentheses{

public static void main(String[] args) {

Solution solution = new ValidParentheses().new Solution();
    System.out.println(solution.isValid("){"));
}

//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    public  boolean  isValid(String s) {
        Deque<Character> deque = new LinkedList<>();
        for(int i=0;i<s.length();i++){
            if(s.charAt(i) == '(' || s.charAt(i) == '[' || s.charAt(i) == '{'){
                deque.push(s.charAt(i));
            }else {
                if(deque.size() == 0){
                    return false;
                }
                if(deque.peek() == '(' && s.charAt(i) != ')'){
                    return false;
                }else if(deque.peek() == '[' && s.charAt(i) != ']'){
                    return false;
                }else if(deque.peek() == '{' && s.charAt(i) != '}'){
                    return false;
                }else {
                    deque.poll();
                    continue;
                }
            }

        }

        return deque.size()>0?false:true;
    }
}
//leetcode submit region end(Prohibit modification and deletion)


}