package com.cn.codebrush.leetcode.editor.cn;

//给你一根长度为 n 的绳子，请把绳子剪成整数长度的 m 段（m、n都是整数，n>1并且m>1），每段绳子的长度记为 k[0],k[1]...k[m - 1]
// 。请问 k[0]*k[1]*...*k[m - 1] 可能的最大乘积是多少？例如，当绳子的长度是8时，我们把它剪成长度分别为2、3、3的三段，此时得到的最大乘
//积是18。 
//
// 答案需要取模 1e9+7（1000000007），如计算初始结果为：1000000008，请返回 1。 
//
// 
//
// 示例 1： 
//
// 输入: 2
//输出: 1
//解释: 2 = 1 + 1, 1 × 1 = 1 
//
// 示例 2: 
//
// 输入: 10
//输出: 36
//解释: 10 = 3 + 3 + 4, 3 × 3 × 4 = 36 
//
// 
//
// 提示： 
//
// 
// 2 <= n <= 1000 
// 
//
// 注意：本题与主站 343 题相同：https://leetcode-cn.com/problems/integer-break/ 
// Related Topics 数学 动态规划 
// 👍 196 👎 0


public class JianShengZiIiLcof{

public static void main(String[] args) {

Solution solution = new JianShengZiIiLcof().new Solution();
    System.out.println(solution.cuttingRope(10));
}


//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    public int cuttingRope(int n) {
        // 基于切绳子的思想，尽可能切多点3，对于最后的切法，要考虑到整体长度
        // 起码得切2，切1没有意义。
        // 或者切3，即绳子长度刚好可以被 3 整除
        // 或者切4，比如一直切3，最后余下1的长度，又因为前述提到不要切1，我们可以把前一段3和这一段1，合并起来，切成4。
        // 不能切5，因为可以将5切成2|3，可以得到乘积6
        if(n == 2)
            return 1;
        if(n == 3)
            return 2;
        long res = 1;
        while(n > 4){
            res *= 3;
            res = res % 1000000007;
            n -= 3;
        }
        // 乘以剩余线段长，只有2、3、4的可能
        return (int)(res * n % 1000000007);
    }
}
//leetcode submit region end(Prohibit modification and deletion)


}