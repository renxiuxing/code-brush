package com.cn.codebrush.leetcode.editor.cn;

//以数组 intervals 表示若干个区间的集合，其中单个区间为 intervals[i] = [starti, endi] 。请你合并所有重叠的区间，并返
//回 一个不重叠的区间数组，该数组需恰好覆盖输入中的所有区间 。 
//
// 
//
// 示例 1： 
//
// 
//输入：intervals = [[1,3],[2,6],[8,10],[15,18]]
//输出：[[1,6],[8,10],[15,18]]
//解释：区间 [1,3] 和 [2,6] 重叠, 将它们合并为 [1,6].
// 
//
// 示例 2： 
//
// 
//输入：intervals = [[1,4],[4,5]]
//输出：[[1,5]]
//解释：区间 [1,4] 和 [4,5] 可被视为重叠区间。 
//
// 
//
// 提示： 
//
// 
// 1 <= intervals.length <= 104 
// intervals[i].length == 2 
// 0 <= starti <= endi <= 104 
// 
// Related Topics 数组 排序 
// 👍 1572 👎 0


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MergeIntervals{

public static void main(String[] args) {

Solution solution = new MergeIntervals().new Solution();
    int[][] intervals = {{1,5},{3,5},{2,6},{8,10},{7,18}};
    System.out.println(solution.merge(intervals));
}

//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    public int[][] merge(int[][] intervals) {
        List<int[]> res = new ArrayList<>();
        if (intervals.length == 0 || intervals == null) return res.toArray(new int[0][]);
        // 对起点终点进行排序
        Arrays.sort(intervals, (a, b) -> a[0] - b[0]);
        for(int i = 0;i < intervals.length;i++){
            int left = intervals[i][0];
            int right = intervals[i][1];
            while(i<intervals.length-1 && intervals[i+1][0] <= right){
                i++;
                right = Math.max(right,intervals[i][1]);
            }
            res.add(new int[]{left,right});

        }
        // 不能直接返回int数组，因为ArrayList里面只能放引用类型，不能放基本类型。
        // 所以只能返回Integer数组，然后转成int数组(不能强转)，因为在java中所有的数组都是引用类型的，
        // 而int和Integer能直接转换是因为jdk会帮我们自己打包解包(包装类)。
        // 但是可以返回int[][]因为java中维的二数组int[][]可以看成存放int[]的数组，而int[]是引用类型。
        // int[0][]指定了返回数组的类型，0是为了节省空间，因为它只是为了说明返回的类型, 并不是真实的长度
        return res.toArray(new int[0][]);
    }
}
//leetcode submit region end(Prohibit modification and deletion)


}