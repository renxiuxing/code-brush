package com.cn.codebrush.leetcode.editor.cn;
// 2022-11-10 10:57:01
// 18  四数之和
//给你一个由 n 个整数组成的数组 nums ，和一个目标值 target 。请你找出并返回满足下述全部条件且不重复的四元组 [nums[a], nums[b
//], nums[c], nums[d]] （若两个四元组元素一一对应，则认为两个四元组重复）： 
//
// 
// 0 <= a, b, c, d < n 
// a、b、c 和 d 互不相同 
// nums[a] + nums[b] + nums[c] + nums[d] == target 
// 
//
// 你可以按 任意顺序 返回答案 。 
//
// 
//
// 示例 1： 
//
// 
//输入：nums = [1,0,-1,0,-2,2], target = 0
//输出：[[-2,-1,1,2],[-2,0,0,2],[-1,0,0,1]]
// 
//
// 示例 2： 
//
// 
//输入：nums = [2,2,2,2,2], target = 8
//输出：[[2,2,2,2]]
// 
//
// 
//
// 提示： 
//
// 
// 1 <= nums.length <= 200 
// -109 <= nums[i] <= 109 
// -109 <= target <= 109 
// 
// Related Topics 数组 双指针 排序 
// 👍 1422 👎 0


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FourSum{

public static void main(String[] args) {

Solution solution = new FourSum().new Solution();
    int[] nums = {2,2,2,2,2};
    int target = 8;
    System.out.println(solution.fourSum(nums,target));
}

//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    public List<List<Integer>> fourSum(int[] nums, int target) {
        int n = nums.length;
        List<List<Integer>> resList = new ArrayList<>();
        Arrays.sort(nums);
        for(int i=0;i<n-3;i++){
            if(target<0 && nums[0] > 0){
                return resList;
            }
            if(target>=0 && nums[n-1]<0){
                return resList;
            }
            if(i>0&&nums[i-1]==nums[i]){continue;}
            for(int j=i+1;j<n;j++){
                if(j>i+1&&nums[j-1]==nums[j]){continue;}
                int l=j+1;
                int r=n-1;
                while(l<r){
                    int sum = nums[i]+nums[j]+nums[l]+nums[r];
                    if(sum == target){
                        resList.add(Arrays.asList(nums[i],nums[j],nums[l],nums[r]));
                        while (l<r && nums[r]==nums[r-1]){r--;}
                        while (l<r && nums[l]==nums[l+1]){l++;}

                        l++;
                        r--;
                    }else if(sum > target){
                        r--;
                    }else {
                        l++;
                    }
                }
            }

        }

        return resList;
    }
}
//leetcode submit region end(Prohibit modification and deletion)


}