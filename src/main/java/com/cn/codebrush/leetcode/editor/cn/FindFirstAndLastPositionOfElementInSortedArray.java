package com.cn.codebrush.leetcode.editor.cn;
// 2022-09-07 11:04:28
// 34  在排序数组中查找元素的第一个和最后一个位置
//给你一个按照非递减顺序排列的整数数组 nums，和一个目标值 target。请你找出给定目标值在数组中的开始位置和结束位置。 
//
// 如果数组中不存在目标值 target，返回 [-1, -1]。 
//
// 你必须设计并实现时间复杂度为 O(log n) 的算法解决此问题。 
//
// 
//
// 示例 1： 
//
// 
//输入：nums = [5,7,7,8,8,10], target = 8
//输出：[3,4] 
//
// 示例 2： 
//
// 
//输入：nums = [5,7,7,8,8,10], target = 6
//输出：[-1,-1] 
//
// 示例 3： 
//
// 
//输入：nums = [], target = 0
//输出：[-1,-1] 
//
// 
//
// 提示： 
//
// 
// 0 <= nums.length <= 105 
// -109 <= nums[i] <= 109 
// nums 是一个非递减数组 
// -109 <= target <= 109 
// 
// Related Topics 数组 二分查找 
// 👍 1887 👎 0


import java.util.Arrays;

public class FindFirstAndLastPositionOfElementInSortedArray{

public static void main(String[] args) {

Solution solution = new FindFirstAndLastPositionOfElementInSortedArray().new Solution();
int[] nums = {1};
    System.out.println(solution.searchRange(nums,1));
}

//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    public int[] searchRange(int[] nums, int target) {
        int n = nums.length;
        int left = 0;
        int right = n;
        int[] res = new int[2];
        Arrays.fill(res,-1);
        while(left<right){
            int mid = (right+left)/2;
            if(nums[mid] == target){
                int tempLeft = mid;
                while(tempLeft>left && nums[tempLeft-1] == target){
                    tempLeft--;
                }
                res[0] = tempLeft;
                int tempRight = mid;
                while(tempRight<right-1 && nums[tempRight+1] == target){
                    tempRight++;
                }
                res[1] = tempRight;
                break;
            }else if(nums[mid] > target){
                right = mid;
            }else {
                left = mid+1;
            }
        }

        return res;
    }
}
//leetcode submit region end(Prohibit modification and deletion)


}