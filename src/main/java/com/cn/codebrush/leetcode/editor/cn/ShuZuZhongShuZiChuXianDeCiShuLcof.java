package com.cn.codebrush.leetcode.editor.cn;
// 2022-09-26 21:10:57
// 剑指 Offer 56 - I  数组中数字出现的次数
//一个整型数组 nums 里除两个数字之外，其他数字都出现了两次。请写程序找出这两个只出现一次的数字。要求时间复杂度是O(n)，空间复杂度是O(1)。 
//
// 
//
// 示例 1： 
//
// 输入：nums = [4,1,4,6]
//输出：[1,6] 或 [6,1]
// 
//
// 示例 2： 
//
// 输入：nums = [1,2,10,4,1,4,3,3]
//输出：[2,10] 或 [10,2] 
//
// 
//
// 限制： 
//
// 
// 2 <= nums.length <= 10000 
// 
//
// 
// Related Topics 位运算 数组 
// 👍 716 👎 0


import org.springframework.http.converter.json.GsonBuilderUtils;

import java.sql.SQLOutput;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

public class ShuZuZhongShuZiChuXianDeCiShuLcof{

public static void main(String[] args) {

Solution solution = new ShuZuZhongShuZiChuXianDeCiShuLcof().new Solution();

}

//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    public int[] singleNumbers(int[] nums) {
        int n = nums.length;
        int[] res  = new int[2];
        Map<Integer,Integer> map = new HashMap<>();
        for(int i=0;i<n;i++){
            if(!map.containsKey(nums[i])){
                map.put(nums[i],1);
            }else {
                map.put(nums[i],map.get(nums[i])+1);
            }

        }
        int k =0;
        for (Integer key : map.keySet()) {
            if(map.get(key) < 2){
                res[k++] = key;
            }
        }

        return res;
        /*int ret = 0;
        for (int n : nums) {
            ret ^= n;
        }
        int div = 1;
        while ((div & ret) == 0) {
            div <<= 1;
        }
        int a = 0, b = 0;
        for (int n : nums) {
            if ((div & n) != 0) {
                a ^= n;
            } else {
                b ^= n;
            }
        }
        return new int[]{a, b};*/
    }

}
//leetcode submit region end(Prohibit modification and deletion)


}