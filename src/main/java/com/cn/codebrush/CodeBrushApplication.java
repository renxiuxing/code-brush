package com.cn.codebrush;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CodeBrushApplication {

    public static void main(String[] args) {
        SpringApplication.run(CodeBrushApplication.class, args);
    }

}
