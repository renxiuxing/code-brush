package com.cn.codebrush.数字;

import java.util.HashMap;
import java.util.LinkedHashMap;

public class No13罗马数字转整数 {

    public int romanToInt(String s) {
        HashMap<String,Integer> map = new LinkedHashMap<>();
        map.put("M",1000);
        map.put("CM",900);
        map.put("D",500);
        map.put("CD",400);
        map.put("C",100);
        map.put("XC",90);
        map.put("L",50);
        map.put("XL",40);
        map.put("X",10);
        map.put("IX",9);
        map.put("V",5);
        map.put("IV",4);
        map.put("I",1);
        int n = s.length();
        int res = 0;
        String[] strs = s.split("");
        for(int i=0;i<n;i++){
            String temp = strs[i];
            if(i+1<n && map.get(strs[i]) < map.get(strs[i+1])){
                temp = strs[i]+strs[i+1];
                i++;
            }
            res = res + map.get(temp);
        }

        return res;
    }
}
