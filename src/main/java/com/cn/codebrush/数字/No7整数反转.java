package com.cn.codebrush.数字;

/**
 * @Author Boolean
 * @Date 2022/7/1 11:57
 * @Version 1.0
 */
public class No7整数反转 {
    public static void main(String[] args) {
        //int res = 1534236469;
        System.out.println(reverse(123));
    }

    public static int reverse(int x) {
        boolean flag = false;
        if(x<0){
            x=-x;
            flag=true;
        }
        int pop = 0;
        int count = x;
        int res = 0;
        while (count != 0){
            //防止溢出，下面需要加上 （*10 + 余数）
            if(res > (Integer.MAX_VALUE - count%10)/10){
                return 0;
            }
            pop = count%10;
            count = count/10;
            res = res*10 + pop;
        }

        return flag?-res:res;
    }
}
