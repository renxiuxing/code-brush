package com.cn.codebrush.数字;

public class No8字符串转换整数atoi {
    public static void main(String[] args) {
        System.out.println(myAtoi("+12"));

    }

    public static int myAtoi(String s) {
        s = s.trim();
        if(s.length() == 0){
            return 0;
        }
        boolean flag = false;
        if(s.charAt(0) == '-'){
            s = s.substring(1);
            flag = true;
        }
        if(s.length() == 0){
            return 0;
        }
        if(s.charAt(0) == '+'){
            if(flag){
                return 0;
            }else{
                s = s.substring(1);
            }
        }
        if(s.length() == 0){
            return 0;
        }
        int res = 0;
        String[] strs = s.split("");
        for(int i=0;i<strs.length;i++){
            if (!Character.isDigit(s.charAt(i))){
                break;
            }
            int temp = Integer.valueOf(strs[i]);
            if(res > (Integer.MAX_VALUE-temp)/10){
                return flag?Integer.MIN_VALUE:Integer.MAX_VALUE;
            }
            res = res*10+temp;
        }

        return flag?-res:res;
    }
}
