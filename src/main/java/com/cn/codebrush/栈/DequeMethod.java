package com.cn.codebrush.栈;

import java.util.Deque;
import java.util.LinkedList;

/**
 * @Author Boolean
 * @Date 2022/5/23 17:06
 * @Version 1.0
 */
public class DequeMethod {
    public static void main(String[] args) {
        Deque<Integer> num = new LinkedList<>();
        num.push(1);
        num.addLast(2);
        num.addFirst(3);//在头部加数据
        num.push(4);//在头部加数据
        num.add(0);//在尾部加数据
        System.out.println(num.getFirst());//返回头部数据，不删除
        System.out.println(num.getLast());//返回尾部收据，不删除

        num.pop();//弹出头部数据,删除

        num.offer(5);//尾部添加数据
        num.offerFirst(6);//头部添加数据
        num.offerLast(7);//尾部添加数据


        System.out.println(num.peek());//返回头部数据，不删除
        System.out.println(num.peekFirst());//返回头部数据，不删除
        System.out.println(num.peekLast());//返回尾部数据，不删除


        System.out.println(num.remove());//删除头部数据，返回
        System.out.println(num.removeFirst());//删除头部数据，返回
        System.out.println(num.removeLast());//删除胃部数据，返回



        System.out.println(num.getLast());//返回尾部数据，不删除
        System.out.println(num.getFirst());//返回头部数据，不删除


        System.out.println(num.poll());//删除头部数据，返回
        System.out.println(num.pollFirst());//删除头部数据，返回
        System.out.println(num.pollLast());//删除尾部数据，返回
    }
}
