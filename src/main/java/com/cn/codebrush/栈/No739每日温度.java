package com.cn.codebrush.栈;

import java.util.Deque;
import java.util.LinkedList;

/**
 * @Author Boolean
 * @Date 2022/6/22 14:37
 * @Version 1.0
 */
public class No739每日温度 {
    public static void main(String[] args) {
         int[] temperatures = {73,74,75,71,69,72,76,73};  //[1,1,4,2,1,1,0,0]
        System.out.println(dailyTemperatures(temperatures));
    }

    public static int[] dailyTemperatures(int[] temperatures) {
        int n = temperatures.length;
        int[] res = new int[n];
        Deque<Integer> deque = new LinkedList<>();
        for(int i=0;i<n;i++){
            int temperature = temperatures[i];
            while(!deque.isEmpty() && temperature > temperatures[deque.peek()]){
                int j = deque.poll();
                res[j] = i - j;
            }
            deque.push(i);
        }

        return res;
    }
}
