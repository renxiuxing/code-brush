package com.cn.codebrush.栈;

import java.util.LinkedList;

/**
 * @Author Boolean
 * @Date 2022/6/15 21:18
 * @Version 1.0
 */
public class No剑指Offer30包含min函数的栈 {
    class MinStack {

        /** initialize your data structure here. */
        private final LinkedList<Integer> stack;
        private final LinkedList<Integer> minStack;

        public MinStack() {
            this.stack = new LinkedList<>();
            this.minStack = new LinkedList<>();
        }

        public void push(int x) {
            if(minStack.isEmpty() || minStack.getLast() >= x){
                minStack.add(x);
            }
            stack.add(x);
        }

        public void pop() {
            int val = stack.removeLast();
            if(val == minStack.getLast()){
                minStack.removeLast();
            }
        }

        public int top() {
            return stack.getLast();
        }

        public int min() {
            return minStack.getLast();
        }
    }
}
