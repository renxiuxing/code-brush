package com.cn.codebrush.字符串;

import java.util.*;

/**
 * @Author Boolean
 * @Date 2022/6/19 21:52
 * @Version 1.0
 */
public class No187重复的DNA序列 {
    public static void main(String[] args) {

    }

    public List<String> findRepeatedDnaSequences(String s) {
        int n = s.length();
        Map map = new HashMap<>();
        Set res = new HashSet<>();
        int left =0,right=left+10;
        while(right < n){
            String temp = s.substring(left,right);
            if(!map.containsKey(temp)){
                map.put(temp,temp);
            }else {
                res.add(temp);
            }
            left++;
            right++;
        }

        return new ArrayList<>(res);

    }
}
