package com.cn.codebrush.字符串;

/**
 * @Author Boolean
 * @Date 2022/5/9 14:53
 * @Version 1.0
 */
public class No942增减字符串匹配 {
    public static void main(String[] args) {
        String s = "IDID";
        String s1 = "III";
        String s2 = "DDI";
        String s3 = "D";
        System.out.println(diStringMatch(s3));
    }

    public static int[] diStringMatch(String s) {
        int n = s.length();
        int i=0,j=n;
        int[] nums = new int[n+1];
        for(int k=0;k<n;k++){
            if(s.charAt(k) == 'I'){
                nums[k] = i;
                i++;
            }else {
                nums[k] = j;
                j--;
            }
        }
        nums[n] = j;
        return nums;
    }
}
