package com.cn.codebrush.字符串;

import java.util.Deque;
import java.util.LinkedList;

/**
 * @Author Boolean
 * @Date 2022/5/28 16:00
 * @Version 1.0
 */
public class No1021删除最外层的括号 {
    public static void main(String[] args) {
        String s = "(()())(())";
        System.out.println(removeOuterParentheses(s));
    }

    public static String removeOuterParentheses(String s) {
        int n = s.length();
        Deque<Character> deque = new LinkedList<>();
        String res = "";
        for(int i=0;i<n;i++){
            char ch = s.charAt(i);
            if(ch == ')'){
                deque.pop();
            }
            if(!deque.isEmpty()){
                res += ch;
            }
            if(ch == '('){
                deque.push(s.charAt(i));
            }
        }
        return res;
    }
}
