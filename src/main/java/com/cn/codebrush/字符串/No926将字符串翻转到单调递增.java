package com.cn.codebrush.字符串;

/**
 * @Author Boolean
 * @Date 2022/6/11 16:56
 * @Version 1.0
 */
public class No926将字符串翻转到单调递增 {
    public static void main(String[] args) {
        String s = "00110";
        System.out.println(minFlipsMonoIncr(s));
    }

    public static int minFlipsMonoIncr(String s) {
        int n = s.length();
        int res = 0;
        int count = 0;
        for(int i=0;i<n;i++){
            if(s.charAt(i) == '1'){
                count++;
            }else {
                res = Math.min(res+1,count);
            }
        }
        return res;
    }
}
