package com.cn.codebrush.字符串;

public class No14最长公共前缀 {




    public String longestCommonPrefix(String[] strs) {
        int n = strs.length;
        String res = strs[0];
        for(int i=1;i<n;i++){
            String temp = strs[i];
            int len = res.length()>temp.length()?temp.length():res.length();
            if(len < res.length()){
                res = res.substring(0,len);
            }
            int j = 0;
            while(j<len){
                if(res.charAt(j) != temp.charAt(j)){
                    res = res.substring(0,j);
                    break;
                }
                j++;
            }
        }

        return res;
    }
}
