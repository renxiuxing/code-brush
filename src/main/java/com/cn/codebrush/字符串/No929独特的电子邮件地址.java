package com.cn.codebrush.字符串;

import java.util.HashSet;
import java.util.Set;

public class No929独特的电子邮件地址 {
    public static void main(String[] args) {
        String[] emails = {"a@leetcode.com","b@leetcode.com","c@leetcode.com"};
        String[] emails1 = {"fg.r.u.uzj+o.pw@kziczvh.com","r.cyo.g+d.h+b.ja@tgsg.z.com","fg.r.u.uzj+o.f.d@kziczvh.com","r.cyo.g+ng.r.iq@tgsg.z.com","fg.r.u.uzj+lp.k@kziczvh.com","r.cyo.g+n.h.e+n.g@tgsg.z.com","fg.r.u.uzj+k+p.j@kziczvh.com","fg.r.u.uzj+w.y+b@kziczvh.com","r.cyo.g+x+d.c+f.t@tgsg.z.com","r.cyo.g+x+t.y.l.i@tgsg.z.com","r.cyo.g+brxxi@tgsg.z.com","r.cyo.g+z+dr.k.u@tgsg.z.com","r.cyo.g+d+l.c.n+g@tgsg.z.com","fg.r.u.uzj+vq.o@kziczvh.com","fg.r.u.uzj+uzq@kziczvh.com","fg.r.u.uzj+mvz@kziczvh.com","fg.r.u.uzj+taj@kziczvh.com","fg.r.u.uzj+fek@kziczvh.com"};
        String[] emails2 = {"test.email+alex@leetcode.com","test.email.leet+alex@code.com"};
        System.out.println(numUniqueEmails(emails2));
    }

    /**
     * '+' 操作需要加转义字符  email.substring(0, i).split("\\+")[0];
     * @param emails
     * @return
     */
    public static int numUniqueEmails(String[] emails) {
        Set set= new HashSet<>();

        for(int i=0;i<emails.length;i++){
            String str1 = emails[i].split("@")[0];
            String str2 = "@" + emails[i].split("@")[1];
            String str3 = str1;
            for(int j=0;j<str1.length();j++){
                if(str1.charAt(j) == '+'){
                    str3 = str1.substring(0,j);
                    break;
                }
            }

            str3 = str3.replace(".","");
            set.add(str3+str2);
        }

        return set.size();
    }
}
