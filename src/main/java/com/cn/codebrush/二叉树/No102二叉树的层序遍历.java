package com.cn.codebrush.二叉树;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;

public class No102二叉树的层序遍历 {
    public static void main(String[] args) {
        TreeNode tree = new TreeNode(2);
        TreeNode a = new TreeNode(1);
        TreeNode b = new TreeNode(3);
        tree.left = a;
        tree.right = b;
        System.out.println(levelOrder(tree));
    }

    public static List<List<Integer>> levelOrder(TreeNode root) {
        if(root == null){
            return new ArrayList<>();
        }

        ArrayDeque<TreeNode> deque = new ArrayDeque();
        List list = new ArrayList();
        deque.add(root);
        while(!deque.isEmpty()){
            List sonList = new ArrayList();
            int count = deque.size();
            while(count > 0){
                TreeNode temp = deque.poll();
                sonList.add(temp.val);
                if(temp.left != null){
                    deque.add(temp.left);
                }
                if(temp.right != null){
                    deque.add(temp.right);
                }
                count--;
            }
            list.add(sonList);
        }
        return list;
    }
}
