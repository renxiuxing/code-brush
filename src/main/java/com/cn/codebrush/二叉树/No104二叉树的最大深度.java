package com.cn.codebrush.二叉树;

/**
 * @Author Boolean
 * @Date 2022/5/5 15:15
 * @Version 1.0
 */
public class No104二叉树的最大深度 {
    public static void main(String[] args) {
        TreeNode tree = new TreeNode(1);
        TreeNode a = new TreeNode(2);
        TreeNode b = new TreeNode(3);
        tree.left = a;
        tree.right = b;

        TreeNode tree2 = new TreeNode(1);
        TreeNode a2 = new TreeNode(2);
        TreeNode b2 = new TreeNode(2);
        TreeNode c2 = new TreeNode(3);
        TreeNode d2 = new TreeNode(4);
        TreeNode e2 = new TreeNode(3);
        TreeNode f2 = new TreeNode(3);
        TreeNode g2 = new TreeNode(3);
        TreeNode h2 = new TreeNode(3);
        tree2.left = a2;
        tree2.right = b2;
        a2.left = c2;
        //a2.right = d2;
        //b2.left = e2;
        //b2.right = f2;

        System.out.println(maxDepth(tree2));
    }

    static int resdep = 1;
    public static int maxDepth(TreeNode root) {
        if(root == null){
            return 0;
        }
        maxDepthHelper(root,1);
        return resdep;
    }
    public static void maxDepthHelper(TreeNode root,int dep) {
        if(root.left == null && root.right == null){
            resdep = dep>resdep?dep:resdep;
        }

        if(root.left != null){
            maxDepthHelper(root.left,dep+1);
        }
        if(root.right != null){
            maxDepthHelper(root.right,dep+1);
        }
    }
}
