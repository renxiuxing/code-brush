package com.cn.codebrush.二叉树;

import java.util.ArrayList;
import java.util.List;

public class No94二叉树的中序遍历 {
    public static void main(String[] args) {
        TreeNode root = new TreeNode(1);
        TreeNode a = new TreeNode(2);
        TreeNode b = new TreeNode(2);
        TreeNode c = new TreeNode(4);
        TreeNode d = new TreeNode(3);
        TreeNode e = new TreeNode(6);
        TreeNode f = new TreeNode(7);
        TreeNode g = new TreeNode(8);

        //root.left = a;
        //root.right = b;
        /*a.left = c;
        c.right = f;*/
       // b.left = d;
        /*b.right = e;
        e.left = g;*/

        System.out.println(inorderTraversal(root));

    }

    static List list = new ArrayList();
    public static List<Integer> inorderTraversal(TreeNode root) {
        if(root == null){
            return list;
        }
        if(root.left != null){
            inorderTraversal(root.left);
        }
        list.add(root.val);
        if(root.right != null){
            inorderTraversal(root.right);
        }
        return list;
    }

}
