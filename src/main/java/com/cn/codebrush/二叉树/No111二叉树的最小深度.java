package com.cn.codebrush.二叉树;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author Boolean
 * @Date 2022/5/5 15:45
 * @Version 1.0
 */
public class No111二叉树的最小深度 {
    public static void main(String[] args) {
        TreeNode tree = new TreeNode(1);
        TreeNode a = new TreeNode(2);
        TreeNode b = new TreeNode(3);
        tree.left = a;
        tree.right = b;

        TreeNode tree2 = new TreeNode(1);
        TreeNode a2 = new TreeNode(2);
        TreeNode b2 = new TreeNode(2);
        TreeNode c2 = new TreeNode(3);
        TreeNode d2 = new TreeNode(4);
        TreeNode e2 = new TreeNode(3);
        TreeNode f2 = new TreeNode(3);
        TreeNode g2 = new TreeNode(3);
        TreeNode h2 = new TreeNode(3);
        tree2.left = a2;
        tree2.right = b2;
        a2.left = c2;
        //a2.right = d2;
        //b2.left = e2;
        //b2.right = f2;

        System.out.println(minDepth(tree2));
    }

    static List<Integer> list = new ArrayList();
    public static int minDepth(TreeNode root) {
        if(root == null){
            return 0;
        }
        minDepthHelper(root,1);
        int dep = list.get(0);
        for(int i=0;i<list.size();i++){
            dep = dep>list.get(i)?list.get(i):dep;
        }
        return dep;
    }
    public static void minDepthHelper(TreeNode root,int dep) {
        if(root.left == null && root.right == null){
            list.add(dep);
        }

        if(root.left != null){
            minDepthHelper(root.left,dep+1);
        }
        if(root.right != null){
            minDepthHelper(root.right,dep+1);
        }
    }
}
