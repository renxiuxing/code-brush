package com.cn.codebrush.练习;

/**
 * @Author Boolean
 * @Date 2022/7/14 10:18
 * @Version 1.0
 */
public class No42接雨水 {
    public static void main(String[] args) {
        int[] nums = {0,1,0,2,1,0,1,3,2,1,2,1};
        System.out.println(trap(nums));
        //System.out.println(grayCode(4));  // 16
        //System.out.println(1^3);

    }

    /**
     * 输入：height = [0,1,0,2,1,0,1,3,2,1,2,1]
     * 输出：6
     * 解释：上面是由数组 [0,1,0,2,1,0,1,3,2,1,2,1] 表示的高度图，在这种情况下，可以接 6 个单位的雨水（蓝色部分表示雨水）。
     *
     * @param
     * @return
     */
    public static int trap(int[] height) {
        int n = height.length;
        int left = 0;
        int right = n-1;
        int leftMax = 0;
        int rightMax = 0;
        int sum  = 0;
        while(left<right){
            //左边高，积水的最大高度由左边决定，所以移动右边
            if(leftMax>rightMax){
                rightMax = Math.max(rightMax,height[right]);
                sum += rightMax - height[right];
                right--;
            }else {
                leftMax = Math.max(leftMax,height[left]);
                sum += leftMax - height[left];
                left++;
            }
        }

        return sum;
    }
}
