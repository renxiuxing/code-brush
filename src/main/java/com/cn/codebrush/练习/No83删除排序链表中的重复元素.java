package com.cn.codebrush.练习;

/**
 * @Author Boolean
 * @Date 2022/6/16 10:58
 * @Version 1.0
 */
public class No83删除排序链表中的重复元素 {
    public static void main(String[] args) {

    }

    public ListNode deleteDuplicates(ListNode head) {
        if(head == null){
            return null;
        }
        ListNode temp = head.next;
        ListNode pre = head;
        while(temp != null){
            if(pre.val == temp.val){
                pre.next=temp.next;
            }else {
                pre = pre.next;
                temp = temp.next;
            }

        }

        return head;
    }
}
