package com.cn.codebrush.练习;

import java.util.*;

/**
 * @Author Boolean
 * @Date 2022/6/12 23:37
 * @Version 1.0
 */
public class No102二叉树的层序遍历 {
    public static void main(String[] args) {

    }

    public List<List<Integer>> levelOrder(TreeNode root) {
        if(root == null){
            return new ArrayList<>();
        }
        ArrayDeque<TreeNode> deque = new ArrayDeque<>();
        List<List<Integer>> res = new ArrayList<>();
        deque.add(root);
        while(!deque.isEmpty()){
            int count = deque.size();
            List resSon = new ArrayList();
            while(count > 0){
                TreeNode temp = deque.poll();
                resSon.add(temp.val);
                if(temp.left != null){
                    deque.add(temp.left);
                }
                if(temp.right != null){
                    deque.add(temp.right);
                }
                count--;
            }
            res.add(resSon);
        }

        return res;
    }
}
