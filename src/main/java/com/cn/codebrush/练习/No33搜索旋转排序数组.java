package com.cn.codebrush.练习;

/**
 * @Author Boolean
 * @Date 2022/5/27 17:59
 * @Version 1.0
 */
public class No33搜索旋转排序数组 {
    public static void main(String[] args) {
        int[] nums = {1,3};
        System.out.println(search(nums,3));
    }

    public static int search(int[] nums, int target) {
        int n = nums.length;
        int left =0,right=n-1;

        while(left <= right){
            int mid = (left+right)/2;
            if(nums[mid] < nums[right]){ //右边有序
                if(nums[mid] <= target && target <= nums[right]){
                    if(target == nums[mid]){
                        return mid;
                    }else if(target > nums[mid]){
                        left = mid+1;
                    }else {
                        right = mid;
                    }
                }else {
                    right = mid;
                }
            }else {  // 左边有序
                if(nums[left] <= target && target <= nums[mid]){
                    if(target == nums[mid]){
                        return mid;
                    }else if(target < nums[mid]){
                        right = mid;
                    }else {
                        left = mid+1;
                    }
                }else {
                    left = mid+1;
                }
            }
        }

        return -1;
    }
}
