package com.cn.codebrush.练习;

public class No11盛最多水的容器 {
    public static void main(String[] args) {
        int[] nums = {1,8,6,2,5,4,8,3,7};
        System.out.println(maxArea(nums));
    }

    public static int maxArea(int[] height) {
        int n = height.length;
        int left = 0;
        int right = n-1;
        int sum  = 0;
        while (left<right){
            sum = Math.max(sum,(right-left)*Math.min(height[left],height[right]));
            if(height[left] > height[right]){
                right--;
            }else {
                left++;
            }
        }

        return sum;
    }
}
