package com.cn.codebrush.练习;

/**
 * @Author Boolean
 * @Date 2022/6/9 15:56
 * @Version 1.0
 */
public class No45跳跃游戏II {
    public static void main(String[] args) {

    }

    public int jump(int[] nums) {
        int n = nums.length;
        if(n == 1){
            return 0;
        }
        int step = 0;
        int end = 0;
        int k = 0;

        for(int i=0;i<n;i++){
            k = Math.max(k,i+nums[i]);
            if(k >= n-1){
                return step+1;
            }
            if(end == i){
                step++;
                end = k;
            }
        }

        return step;
    }
}
