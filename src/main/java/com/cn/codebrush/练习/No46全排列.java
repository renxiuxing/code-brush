package com.cn.codebrush.练习;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author Boolean
 * @Date 2022/5/28 22:30
 * @Version 1.0
 */
public class No46全排列 {
    public static void main(String[] args) {
        int[] nums = {1,2,3};
        System.out.println(permute(nums));
    }

    static List reslist = new ArrayList();
    public static List<List<Integer>> permute(int[] nums) {
        int[] visited = new int[nums.length];
        permuteHelper(nums,new ArrayList(),visited);
        return reslist;
    }

    public static void permuteHelper(int[] nums,List list,int[] visited) {
        int n = nums.length;
        if(list.size() == n){
            reslist.add(new ArrayList<>(list));
            return;
        }

        for(int i=0;i<n;i++){
            if(visited[i] == 1){ continue; }
            visited[i] = 1;
            list.add(nums[i]);
            permuteHelper(nums,list,visited);
            visited[i] = 0;
            list.remove(list.size()-1);
        }
    }
}
