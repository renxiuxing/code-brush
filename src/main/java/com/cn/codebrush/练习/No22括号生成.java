package com.cn.codebrush.练习;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author Boolean
 * @Date 2022/5/23 22:06
 * @Version 1.0
 */
public class No22括号生成 {

    public static void main(String[] args) {
        System.out.println(generateParenthesis(3));
    }

    static List<String> res = new ArrayList<>();
    public static List<String> generateParenthesis(int n) {
        generateParenthesisHelper("",n,n);
        return res;
    }

    public static void generateParenthesisHelper(String curStr,int left,int right) {
        if(left == 0 && right == 0){
            res.add(curStr);
            return;
        }

        if(left < 0 || right <0){
            return;
        }

        if(left > right){
            return;
        }

        generateParenthesisHelper(curStr+"(",left-1,right);
        generateParenthesisHelper(curStr+")",left,right-1);
    }
}
