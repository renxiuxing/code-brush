package com.cn.codebrush.练习;

/**
 * @Author Boolean
 * @Date 2022/5/23 21:40
 * @Version 1.0
 */
public class No21合并两个有序链表 {




    public ListNode mergeTwoLists(ListNode list1, ListNode list2) {

        ListNode res = new ListNode();
        ListNode temp = res;
        while(list1 != null && list2 != null){
            if(list1.val <= list2.val){
                temp.next = new ListNode(list1.val);
                list1 = list1.next;
            }else {
                temp.next = new ListNode(list2.val);
                list2 = list2.next;
            }
            temp = temp.next;
        }

        if(list1 == null){
            temp.next = list2;
        }
        if(list2 == null){
            temp.next = list1;
        }

        return res.next;
    }
}
