package com.cn.codebrush.练习;

import java.util.Arrays;

/**
 * @Author Boolean
 * @Date 2022/5/26 22:36
 * @Version 1.0
 */
public class No31下一个排列 {
    public static void main(String[] args) {
        /*以数字序列 [4,5,2,6,3,1] 为例，下一个排列为：
                    [4,5,3,1,2,6]
        */
    }

    public  void nextPermutation(int[] nums) {
        int n = nums.length;
        for(int i=n-1;i>0;i--){
            if(nums[i] > nums[i-1]){
                Arrays.sort(nums,i,n);
            }
            for(int j=i;j<n;j++){
                if(nums[j] > nums[i-1]){
                    int temp = nums[i-1];
                    nums[i-1] = nums[j];
                    nums[j] = temp;
                    return;
                }
            }
        }
        Arrays.sort(nums);
    }
}
