package com.cn.codebrush.练习;

/**
 * @Author Boolean
 * @Date 2022/6/17 23:04
 * @Version 1.0
 */
public class No807保持城市天际线 {
    public static void main(String[] args) {
        int[][] nums = {{3,0,8,4},{2,4,5,7},{9,2,6,3},{0,3,1,0}};
        System.out.println(maxIncreaseKeepingSkyline(nums));
    }

    public static int maxIncreaseKeepingSkyline(int[][] grid) {
        int n = grid.length;
        int[] rowNum = new int[n];
        int[] colNum = new int[n];

        for(int i=0;i<n;i++){
            for(int j=0;j<n;j++){
                rowNum[i] = Math.max(rowNum[i],grid[i][j]);
                colNum[j] = Math.max(colNum[j],grid[i][j]);
            }
        }

        int res =0;
        for(int i=0;i<n;i++){
            for(int j=0;j<n;j++){
                res += Math.min(rowNum[i],colNum[j]) - grid[i][j];
            }
        }

        return res;
    }
}
