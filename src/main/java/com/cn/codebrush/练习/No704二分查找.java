package com.cn.codebrush.练习;

/**
 * @Author Boolean
 * @Date 2022/5/27 22:22
 * @Version 1.0
 */
public class No704二分查找 {
    public static void main(String[] args) {
        System.out.println(search(new int[]{1},1));
        System.out.println(search(new int[]{1,3},3));
    }

    public static int search(int[] nums, int target) {
        int n = nums.length;
        int left=0,right=n;

        while(left < right){
            int mid = (left+right)/2;
            if(target == nums[mid]){
                return  mid;
            }else if(target > nums[mid]){
                left = mid+1;
            }else {
                right = mid;
            }
        }

        return -1;
    }
}
