package com.cn.codebrush.排序;

public class 计数排序 {
    public static void main(String[] args) {
        int[] nums = {2,4,2,7,2,3,3,8,3};
        nums = countSort(nums);
        for(int i=0;i<nums.length;i++){
            System.out.print(nums[i]);
        }
    }


    public static int[] countSort(int[] nums){
        int n = nums.length;
        int maxVal = nums[0];
        for(int val:nums){
            if(val > maxVal){
                maxVal = val;
            }
        }

        int[] newNums = new int[maxVal+1];
        for(int val:nums){
            newNums[val]++;
        }

        int index = 0;
        for(int i=0;i<newNums.length;i++){
            while(newNums[i] > 0){
                nums[index++] = i;
                newNums[i]--;
            }
        }

        return nums;
    }
}
