package com.cn.codebrush.排序;

/**
 * @Author Boolean
 * @Date 2022/5/29 15:52
 * @Version 1.0
 */
public class 冒泡排序 {
    public static void main(String[] args) {
        int[] nums = {2,4,6,7,1,3,9,8,5};
        nums = bubble(nums);
        for(int i=0;i<nums.length;i++){
            System.out.print(nums[i]);
        }

    }

    public static int[] bubble(int[] nums){
        int n = nums.length;
        for(int i=0;i<n-1;i++){
            for(int j=i+1;j<n;j++){
                if(nums[j] < nums[i]){
                    int temp = nums[j];
                    nums[j] = nums[i];
                    nums[i] = temp;
                }
            }
        }
        return nums;
    }

}
