package com.cn.codebrush.排序;

/**
 * @Author Boolean
 * @Date 2022/7/4 10:14
 * @Version 1.0
 */
public class 选择排序 {
    public static void main(String[] args) {
        int[] nums = {4,2,6,7,1,3,9,8,5};
        nums = selectSort(nums);
        for(int i=0;i<nums.length;i++){
            System.out.print(nums[i]);
        }
    }

    public static int[] selectSort(int[] nums){
        int n= nums.length;
        for(int i=0;i<n;i++){
            int max = i;
            for(int j=i+1;j<n;j++){
                if(nums[j]>nums[max]){
                    max=j;
                }
            }
            if(i != max){
                int temp = nums[i];
                nums[i] = nums[max];
                nums[max] = temp;
            }
        }

        return nums;
    }
}
