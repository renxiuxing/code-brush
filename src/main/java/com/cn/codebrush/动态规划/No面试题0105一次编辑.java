package com.cn.codebrush.动态规划;

/**
 * @Author Boolean
 * @Date 2022/5/13 17:57
 * @Version 1.0
 */
public class No面试题0105一次编辑 {
    public static void main(String[] args) {
        String first = "pales";
        String second = "pale";
        System.out.println(oneEditAway(first,second));
    }

    public static boolean oneEditAway(String first, String second) {
        int len1 = first.length();
        int len2 = second.length();
        int n = len1>len2?len2:len1;
        if(Math.abs(len1-len2) > 1){
            return false;
        }

        int k =0;
        for(int i=0;i<n;i++){
            if(first.charAt(i) != second.charAt(i)){
                if(len1==len2){
                    second = second.substring(0,i)+first.charAt(i)+second.substring(i+1);
                }else if(len1>len2){
                    second = second.substring(0,i)+first.charAt(i)+second.substring(i);
                } else {
                    first = first.substring(0,i)+second.charAt(i)+first.substring(i);
                }
                k++;
                break;
            }
        }


        if(k==0 && Math.abs(len1-len2) == 1){
            return true;
        }

        return first.equals(second);
    }
}
