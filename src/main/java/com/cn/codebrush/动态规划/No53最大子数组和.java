package com.cn.codebrush.动态规划;

/**
 * @Author Boolean
 * @Date 2022/4/27 11:19
 * @Version 1.0
 */
public class No53最大子数组和 {
    public static void main(String[] args) {
        int[] nums = {-2,1,-3,4,-1,2,1,-5,4};
        int[] nums1 ={5,4,-1,7,8};
        int[] nums2 ={1};
        int[] nums3 ={-2,1};
        System.out.println(maxSubArray(nums1));
    }

    //nums = [-2,1,-3,4,-1,2,1,-5,4]
    public static int maxSubArray(int[] nums) {
        int[] dp = new int[nums.length];
        dp[0]=nums[0];
        for(int i=1;i<nums.length;i++){
            dp[i] = dp[i-1]<0?nums[i]:(nums[i]+ dp[i-1]);
        }
        int res = dp[0];
        for(int j=0;j<dp.length;j++){
            //System.out.print(dp[j]);
            res = res<dp[j]?dp[j]:res;
        }
        return res;
    }
}
