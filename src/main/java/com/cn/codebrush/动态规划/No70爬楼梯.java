package com.cn.codebrush.动态规划;

/**
 * @Author Boolean
 * @Date 2022/4/26 15:39
 * @Version 1.0
 */
public class No70爬楼梯 {
    public static void main(String[] args) {
        int n = 1;
        System.out.println(climbStairs(n));
    }

    /**
     * f(1) = 1
     * f(2) = 2
     * f(3) = f(2) + f(1)
     * f(4) = f(3) + f(2)
     */
    public static int climbStairs(int n) {
        if(n == 1){
            return 1;
        }
        if(n == 2){
            return 2;
        }
        int[] dp = new int[n];
        dp[0] = 1;
        dp[1] = 2;

        for(int i=2;i<n;i++){
            dp[i] = dp[i-1] + dp[i-2];
        }
        return dp[n-1];
    }
}
