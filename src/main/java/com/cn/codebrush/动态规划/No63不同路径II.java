package com.cn.codebrush.动态规划;

/**
 * @Author Boolean
 * @Date 2022/4/27 17:36
 * @Version 1.0
 */
public class No63不同路径II {
    public static void main(String[] args) {
        int nums[][] = {{0,0,0}, {0,1,0},{0,0,0}};
        int nums1[][] = {{0,0,0}};
        int nums2[][] = {{0,1,0}};
        int nums3[][] = {{1,0},{0,0}};
        System.out.println(uniquePathsWithObstacles(nums1));
    }


    public static int uniquePathsWithObstacles(int[][] obstacleGrid) {
        int m = obstacleGrid.length;
        int n = obstacleGrid[0].length;
        int[][] dp = new int[m][n];
        dp[0][0] = 1;
        for(int i=0;i<m;i++){
            if(obstacleGrid[i][0] == 1){
                dp[i][0] = 0;
            }else if(i>0){
                dp[i][0] = dp[i-1][0];
            }

            for(int j=0;j<n;j++){
                if(obstacleGrid[0][j] == 1){
                    dp[0][j] = 0;
                }else if(j>0 && i==0){
                    dp[0][j] = dp[i][j-1];
                }
                if(obstacleGrid[i][j] == 1){
                    dp[i][j] = 0;
                }else{
                    if(i>0&&j>0){
                        dp[i][j] = dp[i-1][j] + dp[i][j-1];
                    }
                }

            }
        }

        return dp[m-1][n-1];
    }

}
