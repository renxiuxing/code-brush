package com.cn.codebrush.动态规划;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @Author Boolean
 * @Date 2022/4/28 16:04
 * @Version 1.0
 */
public class No118杨辉三角 {
    public static void main(String[] args) {
        System.out.println(generate(5));
    }

    public static List<List<Integer>> generate(int numRows) {
        List<List<Integer>> resList = new ArrayList<>();
        resList.add(Arrays.asList(1));
        if(numRows == 1){
            return resList;
        }
        resList.add(Arrays.asList(1,1));
        if(numRows == 2){
            return resList;
        }
        for(int i=2;i<numRows;i++){
            List list = new ArrayList();
            list.add(1);
            for(int j=1;j<i;j++){
                list.add(resList.get(i-1).get(j-1)+resList.get(i-1).get(j));
            }
            list.add(1);
            resList.add(list);
        }
        return resList;
    }
}
