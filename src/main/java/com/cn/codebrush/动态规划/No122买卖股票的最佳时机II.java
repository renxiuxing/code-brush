package com.cn.codebrush.动态规划;

/**
 * @Author Boolean
 * @Date 2022/5/15 15:51
 * @Version 1.0
 */
public class No122买卖股票的最佳时机II {
    public static void main(String[] args) {
        int[] prices = {7,1,5,3,6,4};
        int[] prices1 = {7,1,5,6};
        System.out.println(maxProfit(prices));
    }

    public static int maxProfit(int[] prices) {
        int n = prices.length;
        int[] dp = new int[n];

        if(n==1){
            return 0;
        }
        dp[0] = 0;
        for(int i=1;i<n;i++){
            dp[i] = prices[i] - prices[i-1];
        }

        int res = 0;
        for(int j=0;j<n;j++){
            res = res + (dp[j]>0?dp[j]:0);
        }
        return res;
    }
}
