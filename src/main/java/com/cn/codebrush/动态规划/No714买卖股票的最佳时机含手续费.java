package com.cn.codebrush.动态规划;

import java.util.Arrays;

public class No714买卖股票的最佳时机含手续费 {
    public static void main(String[] args) {
        int[] prices = {1, 3, 2, 8, 4, 9};
        int fee = 2;
        System.out.println(maxProfit(prices,fee));
    }

    //prices = [1, 3, 2, 8, 4, 9], fee = 2
    public static int maxProfit(int[] prices, int fee) {
        int n = prices.length;
        int sell = 0;
        int buy = -prices[0];
        for(int i=1;i<n;i++){
            sell = Math.max(sell,buy+prices[i]-fee);
            buy = Math.max(buy,sell - prices[i]);
        }

        return sell;
    }
}
