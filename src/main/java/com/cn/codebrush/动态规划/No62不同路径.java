package com.cn.codebrush.动态规划;

/**
 * @Author Boolean
 * @Date 2022/4/27 15:56
 * @Version 1.0
 */
public class No62不同路径 {
    public static void main(String[] args) {
        //System.out.println(uniquePaths(1,2));
        //System.out.println(uniquePaths(2,2));
        //System.out.println(uniquePaths(2,1));
        //System.out.println(uniquePaths(3,3));
        System.out.println(uniquePaths(3,7));
    }

    /**
     * f(1)(1) = 1
     * f(2)(1) = 1
     * f(1)(2) = 1
     * f(2)(2) = 2
     * f(2)(3) = 3
     * f(3)(2) = 3
     *
     * f(4)(2) = 4
     * f(3)(3) = 4
     * @param m
     * @param n
     * @return
     */
    public static int uniquePaths(int m, int n) {
        if(m<2 || n<2){
            return 1;
        }
        int[][] dp = new int[m][n];
        dp[0][0] = 1;
        dp[0][1] = 1;
        dp[1][0] = 1;
        dp[1][1] = 2;

        for(int i=1;i<m;i++){
            dp[i][0] = 1;
            for(int j=1;j<n;j++){
                dp[0][j] = 1;
                dp[i][j] = dp[i-1][j] + dp[i][j-1];
            }
        }

        return dp[m-1][n-1];
    }
}
