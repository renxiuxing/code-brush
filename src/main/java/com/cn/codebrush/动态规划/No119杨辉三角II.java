package com.cn.codebrush.动态规划;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @Author Boolean
 * @Date 2022/4/28 16:01
 * @Version 1.0
 */
public class No119杨辉三角II {
    public static void main(String[] args) {
        System.out.println(getRow(2));
    }

    public static List<Integer> getRow(int rowIndex) {
        List<List<Integer>> resList = new ArrayList<>();
        resList.add(Arrays.asList(1));
        if(rowIndex == 0){
            return resList.get(0);
        }
        resList.add(Arrays.asList(1,1));
        if(rowIndex == 1){
            return resList.get(1);
        }
        for(int i=2;i<=rowIndex;i++){
            List list = new ArrayList();
            list.add(1);
            for(int j=1;j<i;j++){
                list.add(resList.get(i-1).get(j-1)+resList.get(i-1).get(j));
            }
            list.add(1);
            resList.add(list);
        }
        return resList.get(rowIndex);
    }
}
