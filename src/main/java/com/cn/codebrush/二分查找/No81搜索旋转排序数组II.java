package com.cn.codebrush.二分查找;

public class No81搜索旋转排序数组II {
    public static void main(String[] args) {
        int[] nums = {2,5,6,0,0,1,2};// target = 0
        int[] nums1 = {1,0,1,1,1};// target = 0
        int[] nums2 = {1,1,1,1,1,1,1,1,1,13,1,1,1,1,1,1,1,1,1,1,1,1};// target = 13

        System.out.println(search(nums1,0));
    }

    public static boolean search(int[] nums, int target) {
        int n = nums.length;
        int left =0,right=n-1;
        while (left <= right){
            int mid = (left+right)/2;
            if(nums[mid] == target){
                return true;
            }else if(nums[mid]<nums[right]){
                if(nums[mid]<target && target<=nums[right]){
                    left = mid+1;
                }else {
                    right = mid;
                }
            }else if(nums[mid]>nums[right]){
                if(nums[left]<=target && target<nums[mid]){
                    right = mid;
                }else {
                    left = mid+1;
                }
            }else{
                right--;  //中间值既不大于也不小于，所以只能排除掉这一个元素
            }
        }

        return false;
    }
}
