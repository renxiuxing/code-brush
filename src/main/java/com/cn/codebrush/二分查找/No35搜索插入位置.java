package com.cn.codebrush.二分查找;

/**
 * @Author Boolean
 * @Date 2022/5/9 22:11
 * @Version 1.0
 */
public class No35搜索插入位置 {
    public static void main(String[] args) {
        int[] nums = {1,3,5,6};
        System.out.println(searchInsert(nums,7));
    }

    public static int searchInsert(int[] nums, int target) {
        int n = nums.length;
        int l=0,r=n;
        int res = n;
        while(l<r){
            int mid = (l+r)/2;  //向下取整的，所以操作在右边，右边高一位
            if(nums[mid] >= target){
                res = mid;
                r = mid;
            }else {
                l = mid+1;
            }
        }
        return res;
    }
}
