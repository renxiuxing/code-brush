package com.cn.codebrush.二分查找;

/**
 * @Author Boolean
 * @Date 2022/5/13 11:32
 * @Version 1.0
 */
public class No704二分查找 {
    public static void main(String[] args) {
        int[] nums = {-1,0,3,5,9,12};
        System.out.println(search(nums,9));
    }

    public static int search(int[] nums, int target) {
        int n = nums.length;
        int left = 0;
        int right = n;

        while(left<right){
            int mid = (left+right)/2;
            if(nums[mid] == target){
                return mid;
            }else if(nums[mid] > target){
                right = mid;
            }else {
                left = mid+1;
            }
        }
        return -1;
    }
}
